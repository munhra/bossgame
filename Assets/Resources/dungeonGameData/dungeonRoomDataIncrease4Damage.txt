,Dungeon Room Armor,Dungeon Room Damage,Dungeon Defence Change  Percentage,Dungeon Gold Price,General Point,Main Hability,Group Armor Average,Group Damage Average,Group Defence Chance Average
dungeonRoom_small_1,15,7,3,15,"0,5555555556",Balanced,18,7,"7,333333333"
dungeonRoom_medium_1,18,10,7,20,"0,5833333333",Balanced,,,
dungeonRoom_large_1,21,13,12,22,"0,696969697",Balanced,,,
dungeonRoom_small_2,13,12,4,17,"0,568627451",Damage,15,15,6
dungeonRoom_medium_2,15,15,6,20,"0,6",Damage,,,
dungeonRoom_large_2,17,18,8,23,"0,6231884058",Damage,,,
dungeonRoom_small_3,23,16,7,20,"0,7666666667",Armor,"26,33333333",18,"9,333333333"
dungeonRoom_medium_3,27,18,9,25,"0,72",Armor,,,
dungeonRoom_large_3,29,20,12,26,"0,7820512821",Armor,,,
dungeonRoom_small_4,18,11,11,19,"0,701754386",Balanced,"23,66666667",14,12
dungeonRoom_medium_4,24,13,10,25,"0,6266666667",Balanced,,,
dungeonRoom_large_4,29,18,15,29,"0,7126436782",Balanced,,,
dungeonRoom_small_5,20,12,15,23,"0,6811594203",Defence Chance,"23,33333333","15,33333333","21,33333333"
dungeonRoom_medium_5,23,15,22,27,"0,7407407407",Defence Chance,,,
dungeonRoom_large_5,27,19,27,30,"0,8111111111",Defence Chance,,,
dungeonRoom_small_6,21,22,18,26,"0,7820512821",Attack/Def. Chance,"25,66666667","27,66666667",28
dungeonRoom_medium_6,26,27,29,33,"0,8282828283",Attack/Def. Chance,,,
dungeonRoom_large_6,30,34,37,39,"0,8632478632",Attack/Def. Chance,,,
dungeonRoom_small_7,30,25,20,31,"0,8064516129",Attack/Armor,38,"28,33333333",23
dungeonRoom_medium_7,39,27,22,35,"0,8380952381",Attack/Armor,,,
dungeonRoom_large_7,45,33,27,38,"0,9210526316",Attack/Armor,,,
dungeonRoom_small_8,33,31,22,33,"0,8686868687",Attack,"37,66666667",42,28
dungeonRoom_medium_8,37,45,30,45,"0,8296296296",Attack,,,
dungeonRoom_large_8,43,50,32,47,"0,8865248227",Attack,,,
dungeonRoom_small_9,53,46,37,56,"0,8095238095",Balanced,"59,66666667","54,66666667",41
dungeonRoom_medium_9,59,55,41,59,"0,8757062147",Balanced,,,
dungeonRoom_large_9,67,63,45,63,"0,9259259259",Balanced,,,
dungeonRoom_small_10,57,50,50,70,"0,7476190476",Balanced,"65,33333333",59,53
dungeonRoom_medium_10,66,57,53,73,"0,803652968",Balanced,,,
dungeonRoom_large_10,73,70,56,75,"0,8844444444",Balanced,,,