﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestDungeonGameData
    {
        private DungeonGameData dungeonGameData;

        [UnityTest]
        public IEnumerator TestDungeonGameDataLoadHeroData()
        {
            GameObject gameObject = MonoBehaviour.Instantiate (Resources.Load<GameObject> ("dungeonGameData/dungeonGameData"));
            dungeonGameData = gameObject.GetComponent<DungeonGameData> ();
            Assert.IsNotNull (dungeonGameData);
            dungeonGameData.loadDungeonHeroData ();
            Assert.IsNotNull (dungeonGameData.dungeonHeroDataDict ["DungeonHero_1"]);
            yield return null;
        }

        [UnityTest]
        public IEnumerator TestDungeonGameDataLoadRoomData ()
        {
            GameObject gameObject = MonoBehaviour.Instantiate (Resources.Load<GameObject> ("dungeonGameData/dungeonGameData"));
            dungeonGameData = gameObject.GetComponent<DungeonGameData> ();
            Assert.IsNotNull (dungeonGameData);
            dungeonGameData.loadDungeonRoomData ();
            Assert.IsNotNull (dungeonGameData.dungeonRoomDataDict ["dungeonRoom_medium_8"]);
            DungeonRoomData testDungeonRoom = dungeonGameData.dungeonRoomDataDict ["dungeonRoom_large_7"];
            Assert.AreEqual (33,testDungeonRoom.dungeonRoomDamage);
            yield return null;
        }
    }
}
