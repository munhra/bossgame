﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// TODO : How mutch gold the hero will still from monster.
// TODO : Score Rank.
// TODO : Achivments.
// TODO : Earn some gold when hit the hero.
// TODO : Change the value of the room let's say some classes are in 10, other in 20 and so on. this will cause the player to use cheap classes more often.
// TODO : Include an hero with power up, lets say, super armor, super damage etc.... use tint to diferentiate.
// TODO : Bug when upgrade screen comes in the middle of upgrade screen transition the game stucks, happens on iphone.

public class DungeonGameManager : MonoBehaviour {
	public enum GameState {GAME_MENU, GAME_STARTED, GAME_OVER, GAME_MONSTER_UP, GAME_OVER_SUMMARY};
	public GameState gameState = GameState.GAME_MENU;
	public float roomCollectionTransitionSpeed = 1.0F;
	public GameObject gameBackground;
	public GameObject dungeonLevelManager;
	public DungeonGameSoundManager dungeonSoundManager;

	private Object[] dungeonRoomArray;
	private GameObject dungeonBossRoom;
	private GameObject dungeonTreasureRoom;
	private GameObject dungeonPortalVertical;
	private Vector3[] dungeonRoomPositions;
	public	Object dungeonDefaultRoom;

	public Vector3 heroSpanwPosition = new Vector3 (4.3F, -8.94F, 0.0F);
	public string roomToChange;

	private int totalGold;
	private int totalGoldEarned;

	// Stores the total number of killed heroes between monster upgrade levels
	// on the first level after a monster upgrade level the value will be zeroed
	private int accumulatedKilledHeroesUpgradeLevel = 0;
	private int totalAccumulatedKilledHeroes = 0;
	private int totalAccumulatedKilledHeroesPerLevel = 0;

	public static string[] earnedRoomMonsterClass = new string[18];
	private int currentHeroIndex;

	public float initPositionX = -3.05F;
	public float initPositionY = -6.00F;//-6.00F;
	public float roomYPositionAdjust = 0.13F;

	public float roomSpaceX = 1.32F;
	public float roomSpaceY = 0.84F;

	public Button playGameButton;
	public GameObject dungeonGameUpdate;
	public GameObject dungeonGameOver;
	public GameObject dungeonGameData;
	public GameObject dungeonGameFPSText;

	public bool debugGameOver = false;
	public bool showFPS = true;

	public static bool isClearPlayerPrefs = false;
	// TODO : firstUpdateLevel to 2 when in demo mode
	private int firstUpdateLevel = 10; //10 

    //stores the data of minHeroesDeadToUnlockMonster for the next 3 upgrade
    //the values of this array will be show on the monster upgrade gauge
    private string[] minThreeHeroesToDie = { "0", "0", "0" };
	private string[] unlockThreeDungeonPrefabNames = { "dungeonRooms\\dungeonRoom_small_1", "", "" };

	private void Awake ()
	{
		Screen.SetResolution (360, 640, true);
	}

	void Start () {
		if (DungeonGameManager.isClearPlayerPrefs) {
			DungeonGameManager.clearPlayerPrefs ();
		}
		dungeonSoundManager = GameObject.Find ("dungeonSoundManager").GetComponent<DungeonGameSoundManager> ();
		loadAccumulatedKilledHeroesUpgradeLevel ();
		loadTotalAccumulatedKilledHeroes ();
		loadNextThreeDungeonsForGauge ();
		dungeonDefaultRoom = Resources.Load ("dungeonRooms/dungeonRoom_0") as Object;

		if (playGameButton == null) {
		
		} else {
			playGameButton.onClick.AddListener (playButtonOnClick);
		}

		if (debugGameOver) {
			Invoke("performFakeGameOver",1.5f);
		}
		updateHudLevel ();
		Invoke ("playLogoMusic", 0.1f);

		if (!Debug.isDebugBuild) {
			Destroy (dungeonGameFPSText);
		}
	}

	private void playLogoMusic()
	{
		dungeonSoundManager.playTitleScreenMusic ();
	}

    public static void clearPlayerPrefs() {
		PlayerPrefs.DeleteAll();
    }

    void Update () {
		if (gameState == GameState.GAME_OVER) {
			gameState = GameState.GAME_OVER_SUMMARY;
			totalAccumulatedKilledHeroesPerLevel = 0;
			performGameOver ();
		}
	}

	public int getTotalGold()
	{
		return totalGold;
	}

	/// <summary>
	/// This method is only for test purpouse it needs to be disabled in the release.
	/// </summary>
	void performFakeGameOver () {
        gameState = GameState.GAME_MONSTER_UP;
        CancelInvoke("spawnHeroes");
        dungeonLevelManager.GetComponent<DungeonLevelManager>().totalWaveNumberOfHeroes = 0;
        fetchNextThreeMinTotalHeroesDied();
		dungeonGameUpdate.GetComponent<DungeonGameUpdate>().configureGaugeTexts(minThreeHeroesToDie[0], minThreeHeroesToDie[1], minThreeHeroesToDie[2]);
		dungeonGameUpdate.GetComponent<DungeonGameUpdate>().buildGameOverUpgradeRoomCards (unlockThreeDungeonPrefabNames);
		calculateDungeonRoomToUnlock ();
		Invoke ("setGaugeValue",2.0f);
		Invoke ("setGaugeIndicators", 2.0f);
		moveMonsterUpgradeToScreen ();
    }

	/// <summary>
	/// This method will sabe the accumulated number of killed heroes between
	/// monster upgrade levels, it should be called when the player wins a
	/// level
	/// </summary>
	private void saveAccumulatedKilledHeroesUpgradeLevel()
    {
		PlayerPrefs.SetInt ("AccumulatedKilledHeroesUpgradeLevel", accumulatedKilledHeroesUpgradeLevel);
	}

	private void saveTotalAccumulatedKilledHeroes()
	{
		PlayerPrefs.SetInt ("TotalAccumulatedKilledHeroes", totalAccumulatedKilledHeroes);
	}

    /// <summary>
    /// This method shall be called when the game starts as it will contain the
    /// accumulated killed heroes between upgrade levels
    /// </summary>
    private void loadAccumulatedKilledHeroesUpgradeLevel()
    {
		accumulatedKilledHeroesUpgradeLevel = PlayerPrefs.GetInt ("AccumulatedKilledHeroesUpgradeLevel");
		// TODO : Remove this as this is for debug this is accumulated killed heroes
		// accumulatedKilledHeroesUpgradeLevel = 24;
    }

	private void loadTotalAccumulatedKilledHeroes()
	{
		totalAccumulatedKilledHeroes = PlayerPrefs.GetInt ("TotalAccumulatedKilledHeroes");
	}

	private void setGaugeIndicators()
	{
		float percent1 = float.Parse (minThreeHeroesToDie [0]) / float.Parse (minThreeHeroesToDie [2]);
		float percent2 = float.Parse (minThreeHeroesToDie [1]) / float.Parse (minThreeHeroesToDie [2]);
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().setProgressIndicators (percent1, percent2);
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().setProgressIndicatorValuePosition (percent1, percent2);
	}

	private float calculateKilledHeroesPercentage ()
	{
		return Mathf.Min (accumulatedKilledHeroesUpgradeLevel / float.Parse (minThreeHeroesToDie [2]), 1.0f);
	}

	private void setGaugeValue ()
    {
		Debug.Log ("setGaugeValue accumulatedKilledHeroesUpgradeLevel "+ accumulatedKilledHeroesUpgradeLevel);
		float killedHeroesPercentage = calculateKilledHeroesPercentage ();
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().setProgressGauge(killedHeroesPercentage);
		accumulatedKilledHeroesUpgradeLevel = 0;
		totalAccumulatedKilledHeroesPerLevel = 0;
	}

    private void performGameOver() {
		CancelInvoke ("spawnHeroes");
		Invoke ("moveGameOverToScreen", 2.0f);
	}

	public void performGameResumeAfterGameOver()
	{
		gameState = GameState.GAME_STARTED;
		if (dungeonLevelManager.GetComponent<DungeonLevelManager> ().isLastLevel ()) {
			dungeonLevelManager.GetComponent<DungeonLevelManager> ().currentLevel = 1;
			updateHudLevel ();
		}

		moveGameOverOffScreen ();
		cleanAllHeroes();
		dungeonLevelManager.GetComponent<DungeonLevelManager> ().generateLevelHeroesArray ();
		currentHeroIndex = 0;
		scheduleSpwanHeroes ();
		GameObject bossRoom = GameObject.Find("dungeonRoom_BOSS(Clone)");
		Destroy(bossRoom);
		GameObject clone = Instantiate (dungeonBossRoom) as GameObject;
		if (DungeonRoom.isShowingCardCollection) {
			Transform dungeonCollectionTransform = GameObject.Find ("dungeonColectionBG").GetComponent<Transform> ();
			dungeonCollectionTransform.position = new Vector2 (10.97f, dungeonCollectionTransform.position.y);
			DungeonRoom.isShowingCardCollection = false;
			DungeonRoom.setRayCasterBitmapToDungeonRoom ();
		}
		if (debugGameOver) {
			Invoke ("performFakeGameOver", 1.5f);
		}
		DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
		dungeonGameHud.createBossHearts ();
	}

	public void performGameResumeAfterMonsterUp()
    {
        gameState = GameState.GAME_STARTED;
		moveMonsterUpgradeOffScreen ();
        moveOutPlayButton();
        scheduleSpwanHeroes();
		currentHeroIndex = 0;
		if (debugGameOver)
        {
            Invoke("performFakeGameOver", 1.5f);
        }
    }
		
	public void performGameRestart() {
		gameState = GameState.GAME_STARTED;
		moveMonsterUpgradeOffScreen ();
		scheduleSpwanHeroes ();
		resetAllCastleRooms ();
		cleanAllHeroes ();

		GameObject bossRoom = GameObject.Find ("dungeonRoom_BOSS(Clone)");
		Destroy (bossRoom);
		Instantiate (dungeonBossRoom);

		if (DungeonRoom.isShowingCardCollection) {
			Transform dungeonCollectionTransform = GameObject.Find ("dungeonColectionBG").GetComponent<Transform> ();
			dungeonCollectionTransform.position = new Vector2 (10.97f, dungeonCollectionTransform.position.y);
			DungeonRoom.isShowingCardCollection = false;
			DungeonRoom.setRayCasterBitmapToDungeonRoom ();
		}

		if (debugGameOver) {
			Invoke("performFakeGameOver",1.5f);
		}
	}

	private void moveOutPlayButton() {
		DungeonLogo dungeonLogo = GameObject.Find ("logo").GetComponent<DungeonLogo> ();
		dungeonLogo.moveOutButton ();
	}
		
	void moveGameOverPlayButton() {
		DungeonLogo dungeonLogo = GameObject.Find ("logo").GetComponent<DungeonLogo> ();
	}

	void movePlayButton() {
		DungeonLogo dungeonLogo = GameObject.Find ("logo").GetComponent<DungeonLogo> ();
		dungeonLogo.moveButton ();
	}

	private void moveMonsterUpgradeOffScreen () {
		var iTweenGameOverHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBounce, "time",1.0f, "y", 19.7f);
		iTween.MoveTo (dungeonGameUpdate.gameObject, iTweenGameOverHash);
	}

    private void moveMonsterUpgradeToScreen() {
        var iTweenGameOverHash = iTween.Hash("easetype", iTween.EaseType.easeOutBounce, "time", 1.0f, "y", 0.0f);
        iTween.MoveTo(dungeonGameUpdate.gameObject, iTweenGameOverHash);
    }
		
	private void moveGameOverToScreen() { 
		var iTweenGameOverHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBounce, "time",1.0f, "y", 0.62f);
		iTween.MoveTo (dungeonGameOver.gameObject, iTweenGameOverHash);
		bool isLastLavel = dungeonLevelManager.GetComponent<DungeonLevelManager> ().isLastLevel ();
		dungeonGameOver.GetComponent<DungeonGameOver> ().configureGameOver (isLastLavel, totalGoldEarned, totalAccumulatedKilledHeroes);	
		dungeonSoundManager.stopAudio ();
		if (isLastLavel) {
			dungeonSoundManager.playEndGameMusic ();
		} else {
			dungeonSoundManager.playGameOverMusic ();
		}
	}

	public void moveGameOverOffScreen ()
	{
		var iTweenGameOverHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBounce, "time", 1.0f, "y", 40.0f);
		iTween.MoveTo (dungeonGameOver.gameObject, iTweenGameOverHash);
	}

	private void playButtonOnClick() {
		if (gameState == GameState.GAME_MENU) {
			dungeonSoundManager.stopAudio ();
			dungeonSoundManager.playButtonClick();
			DungeonLogo dungeonLogo = GameObject.Find ("logo").GetComponent<DungeonLogo> ();
			gameState = GameState.GAME_STARTED;
			dungeonLogo.dismissLogo ();
			DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
			dungeonGameHud.showHud ();
			startGame ();
		}
	}
		
	void startGame() {
		GameObject castle = Resources.Load ("dungeonCastle/Castle") as GameObject;
		Instantiate (castle);
		loadInitialRoomMatrix ();
		buildBasicCastle ();
		scheduleSpwanHeroes ();
		loadGoldFromPersistence();
		loadTotalGoldEarnedFromPersistence ();
		loadEarnedRoomsFromPersistence ();
		loadRoomCardsIntoCollection ();
		dungeonSoundManager.playRandomLevelMusic ();
	}


	private void loadRoomCardsIntoCollection()
	{
		DungeonCollection dungeonCollection = GameObject.Find ("dungeonColectionBG").GetComponent<DungeonCollection> () as DungeonCollection;
		dungeonCollection.buildRoomCardCollection ();
	}
	

	private void loadGoldFromPersistence () {
		totalGold = PlayerPrefs.GetInt("Gold");
		if (totalGold < 100){
			totalGold = totalGold + 100;
			totalGoldEarned = totalGoldEarned + 100;
		}
		// TODO : How many gold to start ?
		//totalGold = totalGold + 1000;
		DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
		dungeonGameHud.updateHudGold (totalGold);
	}

	private void saveGoldToPersistence (int earnedGold) {
		totalGold = totalGold + earnedGold;
		PlayerPrefs.SetInt("Gold",totalGold);
	}

	private void loadTotalGoldEarnedFromPersistence()
	{
		totalGoldEarned = PlayerPrefs.GetInt ("TotalGoldEarned");
		Debug.Log ("loadTotalGoldEarnedFromPersistence " + totalGoldEarned);
	}

	private void saveTotalGoldEarned()
	{
		PlayerPrefs.SetInt ("TotalGoldEarned", totalGoldEarned);
	}

    private void loadEarnedRoomsFromPersistence()
    {
        for (int i = 0; i < 18; i++)
        {
            if ((i == 0) &&
                ( (PlayerPrefs.GetString((i + 1).ToString()) == "") ||
                  (PlayerPrefs.GetString((i + 1).ToString()) == DungeonRoom.ROOM_CLASS_LOCKED))
                )
            {
				DungeonGameManager.earnedRoomMonsterClass [i] = "dungeonRooms\\dungeonRoom_small_1";
				continue;
            }
            DungeonGameManager.earnedRoomMonsterClass[i] = PlayerPrefs.GetString((i + 1).ToString()) == ""
                ? DungeonRoom.ROOM_CLASS_LOCKED : PlayerPrefs.GetString((i + 1).ToString());
		}
    }

    public static void saveEarnedRoom(string roomPrefabName)
    {
		string[] splittedPreFab = roomPrefabName.Split (char.Parse ("_"));
		var collectionPosition = splittedPreFab [2];
		PlayerPrefs.SetString(collectionPosition, roomPrefabName);
		DungeonGameManager.earnedRoomMonsterClass [int.Parse (collectionPosition) - 1] = roomPrefabName;
		DungeonCollection dungeonCollection = GameObject.Find ("dungeonColectionBG").GetComponent<DungeonCollection> ();
		dungeonCollection.updateRoomCardCollectionAtPosition (int.Parse(collectionPosition) - 1);
	}

	void scheduleSpwanHeroes () {
		InvokeRepeating ("spawnHeroes", 1, 5.5f);
	}

    public static bool heroNotMarkedToBeRemoved(Object dungeonHeroObject)
    {
		if (dungeonHeroObject == null) {
			return false;
		}
		GameObject dungeonHeroGameObject = (GameObject)dungeonHeroObject;
        DungeonHero dungeonHero = dungeonHeroGameObject.GetComponent<DungeonHero>();
		return !dungeonHero.heroMarkedToBeRemoved;
    }
    
    public void spawnHeroesFromLevelManager() {
		if ((gameState == GameState.GAME_OVER) || (gameState == GameState.GAME_OVER_SUMMARY)) {
			return;
		}

        List<Object> levelHeroesList = dungeonLevelManager.GetComponent<DungeonLevelManager>().levelHeroesArray;

		saveAccumulatedKilledHeroesUpgradeLevel ();
		saveTotalAccumulatedKilledHeroes ();

		if (levelHeroesList.FindAll(heroNotMarkedToBeRemoved).Count == 0)
        {
			accumulatedKilledHeroesUpgradeLevel = accumulatedKilledHeroesUpgradeLevel + totalAccumulatedKilledHeroesPerLevel;
			totalAccumulatedKilledHeroes = totalAccumulatedKilledHeroesPerLevel + totalAccumulatedKilledHeroes;
			totalAccumulatedKilledHeroesPerLevel = 0;
			if (dungeonLevelManager.GetComponent<DungeonLevelManager>().getCurrentLevelData().isMonsterUpLevel)
            {
				// will show monster upgrade screen
				dungeonSoundManager.stopAudio ();
				dungeonSoundManager.playUpgradeScreenMusic ();
				gameState = GameState.GAME_MONSTER_UP;
                CancelInvoke("spawnHeroes");
                dungeonLevelManager.GetComponent<DungeonLevelManager>().totalWaveNumberOfHeroes = 0;

                fetchNextThreeMinTotalHeroesDied();
				dungeonGameUpdate.GetComponent<DungeonGameUpdate>().configureGaugeTexts(minThreeHeroesToDie[0], minThreeHeroesToDie[1], minThreeHeroesToDie[2]);
				dungeonGameUpdate.GetComponent<DungeonGameUpdate>().buildGameOverUpgradeRoomCards (unlockThreeDungeonPrefabNames);
				calculateDungeonRoomToUnlock ();
                
				Invoke ("setGaugeValue", 2.0f);
				Invoke ("setGaugeIndicators", 1.0f);

				moveMonsterUpgradeToScreen ();

            } else {
                instantiateHeroes(levelHeroesList);
            }


			// check if it is the last level, if so change to game state to game over, this will cause the game over
			// screen to be show with the end game config
			if (dungeonLevelManager.GetComponent<DungeonLevelManager> ().isLastLevel ()) {
				gameState = GameState.GAME_OVER;
			} else {
				// if it is not the last level generate another level heroes
				// steps the level counter indicating that a new level is reached
				// creates the new level heroes array
				currentHeroIndex = 0;
				dungeonLevelManager.GetComponent<DungeonLevelManager> ().createLevelHeroArray ();
				updateHudLevel ();
			}
		} else {
			instantiateHeroes (levelHeroesList);
        }
    }
	/// <summary>
	/// This method will control the lockers of the dungeon rooms to upgrade, based on the number of killed heroes and
	/// the minHeroesDeadToUnlockMonster per level
	/// </summary>
	private void calculateDungeonRoomToUnlock()
	{
		int numberToUnlock = 0;
		for (int i = 0; i < 3; i++) {
			Debug.Log ("calculateDungeonRoomToUnlock minThreeHeroesToDie " + minThreeHeroesToDie [i]);
			Debug.Log ("calculateDungeonRoomToUnlock accumulatedKilledHeroesUpgradeLevel " + accumulatedKilledHeroesUpgradeLevel);
			if  (accumulatedKilledHeroesUpgradeLevel >= int.Parse(minThreeHeroesToDie[i])) {
				numberToUnlock++;
			}		
		}
		Debug.Log ("calculateDungeonRoomToUnlock numberToUnlock "+numberToUnlock);
		string prefabName = unlockThreeDungeonPrefabNames [Mathf.Max (0, numberToUnlock - 1)];
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().configureLockedIcons (numberToUnlock);
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().buildGameOverLargeCard (prefabName);
		saveEarnedRoom (prefabName);
	}

    /// <summary>
    /// This method fetches the next 3 minHeroesDeadToUnlockMonster to
    /// populate array minThreeHeroesToDie of size 3 with the values that will
    /// be show on the monster upgrade gauge.
    /// </summary>
    private void fetchNextThreeMinTotalHeroesDied()
    {
        DungeonLevelManager dungeonLevelManagerInstance = dungeonLevelManager.GetComponent<DungeonLevelManager>();
        int gaugeHorizonCounter = 0;
        int levelIndexCounter = dungeonLevelManagerInstance.currentLevel;

        //first check if it is required to update array minThreeHeroesToDie as
        //it is only required when when flag updateMonsterUpgradeGauge is true
        //indicating that the 3 values needs to be updated, this flag will be
        //calculated on the sheet and saved on the text file.
        if (!dungeonLevelManagerInstance.levelDataList[levelIndexCounter].updateMonsterUpgradeGauge)
        {
			//Debug.Log ("Lock all monsters when new set is reached");
			// Lock all monsters when new set is reached.
			//dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().lockAllIcons ();

			return;
        }
		Debug.Log ("update new set of monsters");
		dungeonGameUpdate.GetComponent<DungeonGameUpdate> ().lockAllIcons ();
		int valuesHorizon = 2;
		// demo mode is 2 and full mode is 10 and 20 this value needs to come from txtfile
        // if it is the level 10, it means that it is the first level that
        // requires the array minThreeHeroesToDie to be updated, but just in
        // this case the first position of this array will always be 0 as
        // the player will start with a monster, resulting in 0 dead heroes
        // to acquire this monster.
        if (dungeonLevelManagerInstance.currentLevel == firstUpdateLevel) {
            gaugeHorizonCounter = 1;
        }

        while (gaugeHorizonCounter <= valuesHorizon)
        {
            if (dungeonLevelManagerInstance.levelDataList[levelIndexCounter].isMonsterUpLevel)
            {
                minThreeHeroesToDie[gaugeHorizonCounter] = dungeonLevelManagerInstance.levelDataList[levelIndexCounter].minHeroesDeadToUnlockMonster.ToString();
				unlockThreeDungeonPrefabNames [gaugeHorizonCounter] = "dungeonRooms\\" + dungeonLevelManagerInstance.levelDataList [levelIndexCounter].prefabDungeonRoomName;
				gaugeHorizonCounter++;
			}
			levelIndexCounter++;
        }

		saveNextThreeDungeonsForGauge ();
	}

	/// <summary>
	/// Persist the next three offered dungeons, this values will be loaded at the start of the game
	/// so ths upgrade screen will have the correct values even if the loaded level is not an upgrade level.
	/// </summary>
	private void saveNextThreeDungeonsForGauge ()
	{
		for (int i = 0; i < 3; i++) {
			PlayerPrefs.SetString (i.ToString()+ "_OfferedDungeon", unlockThreeDungeonPrefabNames[i]);
			PlayerPrefs.SetString (i.ToString () + "_minThreeHero", minThreeHeroesToDie [i]);
		}
	}

	/// <summary>
	/// Load the previous three ofered dungeons, this need to be done at the begining of the game
	/// to ensure that the upgrade screen will have the correct values even if the loaded level is
	/// not an upgrade level. When there are no string stored value the default read is empty string ""
	/// </summary>
	private void loadNextThreeDungeonsForGauge ()
	{
		for (int i = 0; i < 3; i++) {
			var storedOffereddDungeonRoom = PlayerPrefs.GetString (i.ToString () + "_OfferedDungeon");
			if (storedOffereddDungeonRoom != "") {
				unlockThreeDungeonPrefabNames [i] = PlayerPrefs.GetString (i.ToString ()+ "_OfferedDungeon");
			}
			var storedMinThreeHero = PlayerPrefs.GetString (i.ToString () + "_minThreeHero");
			if (storedMinThreeHero != "") {
				minThreeHeroesToDie [i] = PlayerPrefs.GetString (i.ToString () + "_minThreeHero");
			}
		}
	}

	private void instantiateHeroes(List<Object> levelHeroesList)
    {
		if (currentHeroIndex < levelHeroesList.Count)
        {
            GameObject firstHero = (GameObject)levelHeroesList[currentHeroIndex];
            DungeonHero firstDungeonHero = firstHero.GetComponent<DungeonHero>();
            firstDungeonHero.heroListIndex = currentHeroIndex;
			//GameObject clone = Instantiate(firstHero);
			GameObject clone = firstHero;
			clone.name = "Hero_" + DungeonHero.heroCounter;
            clone.transform.position = heroSpanwPosition;
            clone.GetComponent<DungeonHero>().heroUniqueID = clone.name;
            clone.GetComponent<DungeonHero>().InitHero(dungeonRoomPositions);
            clone.GetComponent<DungeonHero>().heroListIndex = currentHeroIndex;
            DungeonHero.heroCounter++;
            currentHeroIndex++;
        }
    }

	private void spawnHeroes () {
		if (gameState == GameState.GAME_STARTED) {
			spawnHeroesFromLevelManager ();
		}
	}

	private void loadInitialRoomMatrix ()
	{
		dungeonRoomArray = Resources.LoadAll ("dungeonRooms", typeof (GameObject)) as Object [];
		DungeonGameData data = dungeonGameData.GetComponent<DungeonGameData> ();
		data.loadDungeonRoomData ();
		for (int i = 0; i < dungeonRoomArray.Length; i++) {
			if (dungeonRoomArray [i].name == "dungeonRoom_BOSS") {
				dungeonBossRoom = (GameObject)dungeonRoomArray [i];
			} else if (dungeonRoomArray [i].name == "dungeonRoom_TREASURE") {
				dungeonTreasureRoom = (GameObject)dungeonRoomArray [i];
			} else if (dungeonRoomArray [i].name == "dungeonRoom_PORTALVERTICAL") {
				dungeonPortalVertical = (GameObject)dungeonRoomArray [i];
			} else if ((dungeonRoomArray [i].name == "dungeonRoom_PORTALBAIXO")
				|| (dungeonRoomArray [i].name == "dungeonRoom_0")) {
				continue;
			} else {
				GameObject dungeonHeroGameObject = dungeonRoomArray [i] as GameObject;
				DungeonRoom dungeonRoom = dungeonHeroGameObject.GetComponent<DungeonRoom> ();
				DungeonRoomData roomData = data.dungeonRoomDataDict [dungeonRoom.dungeonID];
				dungeonRoom.dungeonLife = roomData.dungeonLife;
				dungeonRoom.dungeonRoomDamage = roomData.dungeonRoomDamage;
				dungeonRoom.dungeonDefenceChance = roomData.dungeonDefenceChance;
				dungeonRoom.dungeonGoldPrice = roomData.dungeonGoldPrice;
				dungeonRoom.dungeonRoomArmor = roomData.dungeonRoomArmor;
			}
		}
	}

	void buildBasicCastle() {
	
		GameObject dungeonRoom = (GameObject) dungeonRoomArray [0];
		BoxCollider2D box2DCollider = dungeonRoom.GetComponent<BoxCollider2D> ();

		float roomWidth = box2DCollider.size.x;
		float roomHeight = box2DCollider.size.y;
		float roomYAdjust = 0.0f;
		float roomXAdjust = 0.0f;
	
		dungeonRoomPositions = new Vector3[15];

		for (int j = 0; j < 5; j++) {
			for (int i = 2; i >= 0; i--) {
				GameObject clone;
				if (j == 4 && i == 0) {
					dungeonPortalVertical.transform.position = new Vector3 (initPositionX + (roomWidth * i * roomSpaceX) - 0.95f, 
						initPositionY + (roomHeight * j * roomSpaceY) + roomYPositionAdjust + 0.15f, 0);
					clone = Instantiate (dungeonPortalVertical) as GameObject;
					roomYAdjust = -1.10f;
					roomXAdjust = -0.20f;
				} else if (j == 4 && i == 1) {
					dungeonTreasureRoom.transform.position = new Vector3 (initPositionX + (roomWidth * i * roomSpaceX) - 1.4f, 
						initPositionY + (roomHeight * j * roomSpaceY) + roomYPositionAdjust - 0.5f, 0);
					clone = Instantiate (dungeonTreasureRoom) as GameObject;
					roomYAdjust = -0.50f;
					roomXAdjust = -0.70f;
				} else if (j == 4 && i == 2) {
					dungeonBossRoom.transform.position = new Vector3 (initPositionX + (roomWidth * i * roomSpaceX) - 1.05f, 
						initPositionY + (roomHeight * j * roomSpaceY) + roomYPositionAdjust + 0.2f, 0);
					clone = Instantiate (dungeonBossRoom) as GameObject;
					roomXAdjust = 0.70f;
					roomYAdjust = -1.2f;
				} else {
					dungeonRoom.transform.position = new Vector3 (initPositionX + (roomWidth * i * roomSpaceX), 
						initPositionY + (roomHeight * j * roomSpaceY) + roomYPositionAdjust, 0);
					clone = Instantiate (dungeonRoom) as GameObject;
					clone.name = "castledungeonRoom_"+((2 - i) + (3 * j));
					roomYAdjust = -0.79f;
				}
				Vector3 roomPositionYAdjusted = clone.transform.position;
				roomPositionYAdjusted.y = roomPositionYAdjusted.y + roomYAdjust; 
				roomPositionYAdjusted.x = roomPositionYAdjusted.x + roomXAdjust;
				dungeonRoomPositions [(2 - i) + (3 * j)] = roomPositionYAdjusted;
			}
		}
	}
		
	public GameObject searchRoomCollectionByName(string nameToSearch) {
		for (int i = 0; i < dungeonRoomArray.Length; i++) {
			if (((GameObject)dungeonRoomArray [i]).name == nameToSearch) {
				return (GameObject)dungeonRoomArray [i];
			}
		}
		return null;
	}

	public void cleanAllHeroes() {
		for (int i = 1; i <= DungeonHero.heroCounter; i++) {
			GameObject hero = GameObject.Find ("Hero_"+i);
			Destroy (hero);
		}
		DungeonHero.heroCounter = 0;
	}

	public void resetAllCastleRooms() {
		var castleRoomName = "";
		for (int i = 0; i < 12; i++) {
			GameObject clone = Instantiate(dungeonDefaultRoom) as GameObject;
			castleRoomName = "castledungeonRoom_"+i;
			clone.name = castleRoomName;
			GameObject oldCastleRoom = GameObject.Find (castleRoomName);
			clone.transform.position = oldCastleRoom.transform.position;
			Destroy (oldCastleRoom);
		}
	}

	public void changeCastleRoom(GameObject newCastleRoom, string oldCastleRoomName, string newCaslteRoomMonsterClass) {
		GameObject oldCastleRoom = GameObject.Find (oldCastleRoomName);
		newCastleRoom.transform.position = oldCastleRoom.transform.position;
		GameObject clone = Instantiate (newCastleRoom);
		clone.name = oldCastleRoomName;
        clone.GetComponent<DungeonRoom>().monsterClass = newCaslteRoomMonsterClass;
        Destroy (oldCastleRoom);
	}

	public void updateHeroGold(int heroGold) {
		saveGoldToPersistence (heroGold);
		DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
		dungeonGameHud.updateHudGold (totalGold);
		if (heroGold > 0) {
			totalGoldEarned += heroGold;
			saveTotalGoldEarned ();
		}
	}

	private void updateHudLevel() {
		DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
		dungeonLevelManager.GetComponent<DungeonLevelManager> ().loadLastLevelFromPersistence ();
		var currentLevel = dungeonLevelManager.GetComponent<DungeonLevelManager> ().currentLevel;
		dungeonGameHud.updateRemaingingDays (currentLevel);
	}

	public void updateKilledHero()
	{
		totalAccumulatedKilledHeroesPerLevel += 1;
	}
}
