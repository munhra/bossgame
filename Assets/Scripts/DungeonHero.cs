﻿using System.Collections.Generic;
using UnityEngine;

public class DungeonHero : MonoBehaviour {
    public enum HeroState { IDLE, START_MOVE, MOVING, ATTACKING, DEFENDING, DIEING };
    private Animator heroAnimator;
    public int heroTotalDamage;
    public int heroLife;
    public int heroGold;
    public int heroNumber;
    public int heroDefenceChance;
    public float heroSpeed;
    public Vector3[] heroDestinies;
    public HeroState heroState = HeroState.IDLE;
    public float heroStartMoveTime;
    public float heroJorneyLength;
    public float heroCoveredDistance;
    public float heroFractionedDistance;
    public int heroActualDestinyIndex;
    public float heroIdleTime;
    public float heroDieTime;
    public float heroAttackTime;
    public float heroHitTime;
    public float heroDefendTime;
    static public int heroCounter;
    static public int levelDiedHeroCounter;
    public bool heroMarkedToBeRemoved = false;
    public string heroUniqueID;
    public DungeonRoom heroDungeonRoom;
    public bool heroAtLevelDoor;
    public int attackTriesCounter;
    public int heroListIndex;
    public int maxAttackTries = 3;
    public string heroName;
    public DungeonGameSoundManager dungeonSoundManager;

    public Vector3[] heroReturnPath = {
        new Vector3 (1.95f, -7.85f),
        new Vector3(-0.29f, -7.94f),
        new Vector3(-2.41f, -8.27f),
        new Vector3(-4.81f, -9.12f)
    };

    public Vector3[] coinAnimationPath1 = {
        new Vector3 (-2.49f, 5.71f),
        new Vector3 (-2.00f, 6.36f),
        new Vector3 (-2.76f, 6.76f),
        new Vector3 (-3.24f, 5.90f),
        new Vector3 (-3.76f, 5.10f)
    };

    public Vector3[] coinAnimationPath2 = {
        new Vector3 (-1.49f, 5.71f),
        new Vector3 (-2.00f, 6.36f),
        new Vector3 (-2.76f, 6.76f),
        new Vector3 (-3.24f, 5.90f),
        new Vector3 (-3.76f, 5.10f)
    };

    void Start() {
        heroAnimator = this.GetComponent<Animator>();
        dungeonSoundManager = GameObject.Find ("dungeonSoundManager").GetComponent<DungeonGameSoundManager> ();
        setupAnimatorController ();
    }

    void Update() {
        if (heroState == HeroState.MOVING) {
            MoveHero(heroDestinies[heroActualDestinyIndex]);
        }
    }

    public void InitHero(Vector3[] destinies) {
        adjustHeroDestinies(destinies);
        heroState = HeroState.IDLE;
        heroActualDestinyIndex = 0;
        Invoke("StartMoveHero", heroIdleTime);
    }

    private void adjustHeroDestinies(Vector3[] destinies) {
        heroDestinies = new Vector3[destinies.Length];
        float randomX = Random.Range(-0.2f, 0.2f);
        float randomY = Random.Range(0, 0.2f);
        for (int i = 0; i < destinies.Length; i++) {
            heroDestinies[i] = new Vector3(destinies[i].x, destinies[i].y, destinies[i].z);
            heroDestinies[i].y = heroDestinies[i].y + 0.35f - randomY;
            heroDestinies[i].x = heroDestinies[i].x + randomX;
        }
    }

    private void StartMoveHero() {
        heroAtLevelDoor = false;
        Vector3 destiny = heroDestinies[heroActualDestinyIndex];
        heroStartMoveTime = Time.time;
        heroJorneyLength = Vector2.Distance(this.transform.position, destiny);
        heroState = HeroState.MOVING;
    }

    private void setupAnimatorController() {
        HeroAnimationClipOverrides clipOverrides;
        Sprite[] heroSprites = Resources.LoadAll<Sprite>("Sprites\\heroes\\hero" + heroNumber);
        Animator dungeonHeroAnimator = GetComponent<Animator>();
        AnimatorOverrideController dungeonHeroOverrideAnimator =
            new AnimatorOverrideController(dungeonHeroAnimator.runtimeAnimatorController);

        clipOverrides = new HeroAnimationClipOverrides(dungeonHeroOverrideAnimator.overridesCount);
        dungeonHeroOverrideAnimator.GetOverrides(clipOverrides);

        for (int j = 0; j < (heroSprites.Length); j++) {

            string filename = "";
            string animationNumber = "";

            if (heroNumber < 10) {
                animationNumber = "0" + heroNumber;
            } else {
                animationNumber = "" + heroNumber;
            }

            switch (j) {

                case 0:
                    filename = heroNumber + "\\Hero " + animationNumber + " attack";
                    clipOverrides["Hero01Attack"] =
                        Resources.Load<AnimationClip>("Animations\\Animations\\dungeonHeroAnimation\\DungeonHero" + filename);
                    break;
                case 1:
                    filename = heroNumber + "\\Hero " + animationNumber + " dead";
                    clipOverrides["Hero01Die"] =
                        Resources.Load<AnimationClip>("Animations\\Animations\\dungeonHeroAnimation\\DungeonHero" + filename);
                    break;
                case 2:
                    filename = heroNumber + "\\Hero " + animationNumber + " defence";
                    clipOverrides["Hero01Defend"] =
                        Resources.Load<AnimationClip>("Animations\\Animations\\dungeonHeroAnimation\\DungeonHero" + filename);
                    break;
                case 3:
                    filename = heroNumber + "\\Hero " + animationNumber + " hit";
                    clipOverrides["Hero01Hit"] =
                        Resources.Load<AnimationClip>("Animations\\Animations\\dungeonHeroAnimation\\DungeonHero" + filename);
                    break;
                case 4:
                    filename = heroNumber + "\\Hero " + animationNumber + " idle";
                    clipOverrides["Hero01Idle"] =
                        Resources.Load<AnimationClip>("Animations\\Animations\\dungeonHeroAnimation\\DungeonHero" + filename);
                    break;
            }

        }

        dungeonHeroOverrideAnimator.ApplyOverrides(clipOverrides);
        dungeonHeroAnimator.runtimeAnimatorController = dungeonHeroOverrideAnimator;
    }

    private void MoveHero(Vector3 destiny) {

        if (!heroAtLevelDoor && (heroActualDestinyIndex > 0) && (heroActualDestinyIndex) % 3 == 0) {

            Vector3 endRoomPosition = heroDestinies[heroActualDestinyIndex - 1];
            endRoomPosition.x = endRoomPosition.x + (endRoomPosition.x * 0.65f);

            if (this.transform.position == endRoomPosition) {
                Vector3 nextLevelDoorPosition = destiny;
                nextLevelDoorPosition.x = nextLevelDoorPosition.x + (nextLevelDoorPosition.x * 0.5f);
                this.transform.position = nextLevelDoorPosition;
                heroAtLevelDoor = true;
            } else {
                float endRoomDistance = Vector2.Distance(this.transform.position, endRoomPosition);
                float endRoomCoveredDistance = (Time.time - heroStartMoveTime) * heroSpeed;
                float endRoomFracDistance = endRoomCoveredDistance / endRoomDistance;
                this.transform.position = Vector2.Lerp(this.transform.position, endRoomPosition, endRoomFracDistance);
            }

        } else {
            heroCoveredDistance = (Time.time - heroStartMoveTime) * heroSpeed;
            heroFractionedDistance = heroCoveredDistance / heroJorneyLength;
            this.transform.position = Vector2.Lerp(this.transform.position, destiny, heroFractionedDistance);
        }

        if (this.transform.position == destiny) {
            heroState = HeroState.ATTACKING;
            if (heroActualDestinyIndex < 12) {
                heroDungeonRoom = GameObject.Find("castledungeonRoom_" + heroActualDestinyIndex).GetComponent<DungeonRoom>();
                if ((heroDungeonRoom.dungeonID == "dungeonRoom_0") || (heroDungeonRoom.isDungeonRoomDead)) {
                    IdleHero();
                } else {
                    attackHero();
                }
            } else if (heroActualDestinyIndex == 12) {
                heroDungeonRoom = GameObject.Find("dungeonRoom_BOSS(Clone)").GetComponent<DungeonRoom>();
                attackHero();
            } else if (heroActualDestinyIndex == 13) {
                IdleHero();
                openPortal();
            } else if (heroActualDestinyIndex == 14) {
                IdleHero();
            }
        }
    }

    void attackHero() {
        attackTriesCounter++;
        heroAnimator.SetBool("heroDefending", false);
        heroAnimator.SetBool("heroHitBoolean", false);
        heroAnimator.SetBool("playerChopBoolean", true);
        Invoke("defendHero", heroAttackTime);
        if (heroActualDestinyIndex < 12) {
            heroDungeonRoom.roomDefend(heroTotalDamage, "castledungeonRoom_" + heroActualDestinyIndex);
        } else if (heroActualDestinyIndex == 12) {
            heroDungeonRoom.roomDefend(heroTotalDamage, "dungeonRoom_BOSS");
        }
        dungeonSoundManager.playHeroAttack ();
        heroState = HeroState.DEFENDING;
    }

    void defendHero() {
        heroAnimator.SetBool("playerChopBoolean", false);
        if (attackTriesCounter == maxAttackTries) {
            heroDungeonRoom.roomIdle();
            attackTriesCounter = 0;
            heroState = HeroState.IDLE;
            Invoke("IdleHero", heroIdleTime);
        } else if (heroDungeonRoom.isDungeonRoomDead) {
            heroState = HeroState.IDLE;
            Invoke("IdleHero", heroIdleTime);
        } else if (heroLife <= 0) {
            heroState = HeroState.DIEING;
            heroAnimator.SetBool("heroDead", true);
            heroDungeonRoom.roomIdle();
            dungeonSoundManager.playHeroDying ();
            updateGameManagerWithHeroKill ();
            Invoke ("destroyHero", heroDieTime);
        } else {
            int randomValue = Random.Range(0, 100);
            int roomDamagePoints = heroDungeonRoom.roomAttack();
            if (randomValue < heroDefenceChance) {
                heroAnimator.SetBool("heroDefending", true);
                dungeonSoundManager.playHeroDefend ();
                Invoke("attackHero", heroDefendTime);
            } else {
                heroAnimator.SetBool("heroHitBoolean", true);
                heroLife = heroLife - roomDamagePoints;
                Invoke("attackHero", heroHitTime);
            }
        }
    }

    private void updateGameManagerWithHeroKill()
	{
        DungeonGameManager dungeonGameManager = GameObject.Find("GameManager").GetComponent<DungeonGameManager>();
        dungeonGameManager.updateHeroGold(this.heroGold);
	    dungeonGameManager.updateKilledHero();
	}

    private void destroyHero() {
        GameObject heroToDie = GameObject.Find(heroUniqueID);
        removeHeroFromLevelList(heroToDie.GetComponent<DungeonHero>());
        Destroy(heroToDie);
    }

    private void removeHeroFromLevelList(DungeonHero heroToDie)
    {
		if (heroToDie == null) {
			return;
		}
		DungeonGameManager dungeonGameManager = GameObject.Find("GameManager").GetComponent<DungeonGameManager>();
        DungeonLevelManager dungeonLevel = dungeonGameManager.dungeonLevelManager.GetComponent<DungeonLevelManager>();
        //heroToDie.heroMarkedToBeRemoved = true;
        (dungeonLevel.levelHeroesArray[heroToDie.heroListIndex] as GameObject).GetComponent<DungeonHero>().heroMarkedToBeRemoved = true;
        // check if all heroes are marked to be removed, if so spawn another level heroes array.
        if (dungeonLevel.levelHeroesArray.FindAll(DungeonGameManager.heroNotMarkedToBeRemoved).Count == 0)
        {
            CancelInvoke ("spawnHeroes");
            dungeonGameManager.spawnHeroesFromLevelManager();
        }
    }

	void IdleHero() {
		heroActualDestinyIndex++;
		if (heroActualDestinyIndex < heroDestinies.Length) {
			Invoke ("StartMoveHero", heroIdleTime);
		}
	}

	void openPortal() {
		DungeonRoom dungeonPortalRoom = GameObject.Find ("dungeonRoom_PORTALVERTICAL(Clone)").GetComponent<DungeonRoom> ();
		GameObject portal = dungeonPortalRoom.gameObject;
		Vector3 portalScale = new Vector3 (1.0f, 1.0f, 0.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",portalScale);
		iTween.ScaleTo(portal, iTweenHeartScaleHash);
		Invoke ("heroFadeOut", 1.0f);
        dungeonSoundManager.playPortalGameplay ();
        openVilagePortal();
	}

	void heroFadeOut() {
		var iTweenHeroFadeOutHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 0.0f);
		iTween.FadeTo (this.gameObject, iTweenHeroFadeOutHash);
		Invoke ("closePortal", 1.2f);
		Invoke ("heroFadeIn", 1.2f);
	}

	void heroFadeIn() {
		this.gameObject.transform.position = new Vector3 (2.65f, -8.03f, 0.0f);
		var iTweenHeroFadeInHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 1.0f);
		iTween.FadeTo (this.gameObject, iTweenHeroFadeInHash);
		Invoke ("heroReturnHome", 1.1f);
	}

	void openVilagePortal() {
		DungeonRoom dungeonPortalRoom = GameObject.Find ("dungeonRoom_PORTALBAIXO").GetComponent<DungeonRoom> ();
		GameObject portal = dungeonPortalRoom.gameObject;
		Vector3 portalScale = new Vector3 (0.5f, 0.5f, 0.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",portalScale);
		iTween.ScaleTo(portal, iTweenHeartScaleHash);
		Invoke ("closeVilagePortal", 3.5f);
	}

	void closeVilagePortal() {
		DungeonRoom dungeonPortalRoom = GameObject.Find ("dungeonRoom_PORTALBAIXO").GetComponent<DungeonRoom> ();
		GameObject portal = dungeonPortalRoom.gameObject;
		Vector3 portalScale = new Vector3 (0.0f, 0.0f, 0.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",portalScale);
		iTween.ScaleTo(portal, iTweenHeartScaleHash);
	}

	private void heroReturnHome() {
		var iTweenValuesHash = iTween.Hash ("easetype", iTween.EaseType.linear, "time",1.5f, "path", heroReturnPath);
		iTween.MoveTo (this.gameObject, iTweenValuesHash);
		iTween.Init(this.gameObject);
		Invoke ("retireHero", 1.6f);
	}

	void chestJumpCoinAnimation(Vector3 chestPosition) {
		GameObject singleCoin = this.gameObject.transform.GetChild (0).gameObject;
		Vector3 singleCoinsScale = new Vector3 (1.0f, 1.0f, 1.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",singleCoinsScale);
		iTween.ScaleTo(singleCoin, iTweenHeartScaleHash);
	}

	void openChestMultipleCoinAnimation(GameObject chest) {
		GameObject multipleCoins = chest.transform.GetChild (0).gameObject;
		Vector3 multipleCoinsScale = new Vector3 (1.0f, 1.0f, 1.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",multipleCoinsScale);
		iTween.ScaleTo(multipleCoins, iTweenHeartScaleHash);
		chestJumpCoinAnimation (multipleCoins.gameObject.transform.position);
	}

	void closeChestMultipleCoinAnimation(GameObject chest) { 
		GameObject multipleCoins = chest.transform.GetChild (0).gameObject;
		Vector3 multipleCoinsScale = new Vector3 (0.0f, 0.0f, 0.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.spring, "time",0.2f,"scale",multipleCoinsScale);
		iTween.ScaleTo(multipleCoins, iTweenHeartScaleHash);
	}

	void retireHero() {
        GameObject heroToDie = GameObject.Find (heroUniqueID);
		if (heroToDie == null) {
			return;
		}
		if (heroToDie.GetComponent<DungeonHero>() == null) {
			return;
		}
		if (heroToDie.GetComponent<DungeonHero> ().heroState == HeroState.DIEING) {
			return;
		}
		removeHeroFromLevelList (heroToDie.GetComponent<DungeonHero>());
        Destroy (heroToDie);
	}

	void closePortal() {
		DungeonRoom dungeonPortalRoom = GameObject.Find ("dungeonRoom_PORTALVERTICAL(Clone)").GetComponent<DungeonRoom> ();
		GameObject portal = dungeonPortalRoom.gameObject;
		Vector3 portalScale = new Vector3 (0.0f, 0.0f, 0.0f);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",0.2f,"scale",portalScale);
		iTween.ScaleTo(portal, iTweenHeartScaleHash);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "dungeonChest") {
			Animator dungeonChest = coll.gameObject.GetComponent<Animator> ();
			dungeonChest.SetBool ("openChest", true);
			openChestMultipleCoinAnimation (coll.gameObject);
            dungeonSoundManager.playOpenChest ();
		}
		if (coll.gameObject.tag == "entranceDoor") {
			Animator entranceDoorAnimator = coll.gameObject.GetComponent<Animator> ();
			entranceDoorAnimator.SetBool ("DoorOpen", true);
		}
		if (coll.gameObject.tag == "roomDoor") {
			Animator roomDoorAnimator = coll.gameObject.GetComponent<Animator> ();
			roomDoorAnimator.SetBool ("OpenDoor", true);
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject.tag == "dungeonChest") {
			Animator dungeonChest = coll.gameObject.GetComponent<Animator> ();
			dungeonChest.SetBool ("openChest", false);
			closeChestMultipleCoinAnimation (coll.gameObject);
		}
		if (coll.gameObject.tag == "entranceDoor") {
			Animator entranceDoorAnimator = coll.gameObject.GetComponent<Animator> ();
			entranceDoorAnimator.SetBool ("DoorOpen", false);
		}
		if (coll.gameObject.tag == "roomDoor") {
			Animator roomDoorAnimator = coll.gameObject.GetComponent<Animator> ();
			roomDoorAnimator.SetBool ("OpenDoor", false);
		}
	}
}

public class HeroAnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
{
	public HeroAnimationClipOverrides(int capacity) : base(capacity) {}
	public AnimationClip this[string name]
	{
		get { 

			return this.Find(x => x.Key.name.Equals(name)).Value; 
		}
		set
		{
			int index = this.FindIndex(x => x.Key.name.Equals(name));
			if (index != -1)
				this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
		}
	}
}