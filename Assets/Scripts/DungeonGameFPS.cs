﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DungeonGameFPS : MonoBehaviour {
	Text fpsText;
	private float deltaTime;

	// Use this for initialization
	void Start () {
		fpsText = GetComponentInChildren<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		float fps = 1.0f / deltaTime;
		fpsText.text = Mathf.Ceil (fps).ToString ();
	}
}
