﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class DungeonRoom : MonoBehaviour, IPointerClickHandler {
    public const string ROOM_CLASS_LOCKED = "Locked";
    public const string ROOM_CLASS_SMALL = "small";
    public const string ROOM_CLASS_MEDIUM = "medium";
    public const string ROOM_CLASS_LARGE = "large";

    public int dungeonRoomArmor;
	public int dungeonRoomDamage;
	public int dungeonLife;
	public int dungeonDefenceChance;
	public int dungeonGoldPrice;
	public string dungeonID;
	public Object dungeonDefaultRoom;
	public Object dungeonHud;
	public bool dungeonClicked;
	public bool dungeonCreatedFromUpdate;

	//Game Over screen values for each room
	public string roomName;
    public string monsterClass;
	//public int unlockPoints;
	public int roomLevel;

	public float roomCollectionDistCovered;
	public float roomShowCollectionFracJorney;
	public float roomHideCollectionFrecJorney;
	public float roomCollectionTranStartTime;
	public float roomCollectionTransitionSpeed = 1.0F;
	public float roomCollectionJorneyLength;

	private const int DUNGEON_ROOM_EVENT_MASK = 2048;
	private const int DUNGEON_CARD_EVENT_MASK = 512;

	public GameObject dungeonCollectionSprite;
	public GameObject gameBackground;
	public DungeonGameSoundManager dungeonSoundManager;

	public static bool isShowingCardCollection;
	public static bool forceHideCardCollection;
	public int dungeonNumber;

	public Vector3 dungeonCollectionOriginalPosition;

	private Animator dungeonRoomAnimator;
	public bool isDungeonRoomDead = false;

	void Start() {
		dungeonLife = dungeonRoomArmor;
		dungeonDefaultRoom = Resources.Load ("dungeonRooms/dungeonRoom_0") as Object;
		dungeonHud = Resources.Load ("dungeonRoomHud/dungeonRoomHud");
		gameBackground = GameObject.Find ("background");
		dungeonSoundManager = GameObject.Find ("dungeonSoundManager").GetComponent<DungeonGameSoundManager> ();

		dungeonCollectionSprite = GameObject.Find ("dungeonColectionBG");
		dungeonCollectionOriginalPosition = dungeonCollectionSprite.transform.position;

		if (dungeonCreatedFromUpdate) {
			dungeonCollectionOriginalPosition.x = 10.97f;
		}

		dungeonRoomAnimator = this.GetComponent<Animator> ();

		if (dungeonNumber > 0) {
			setupAnimatorController();
		}

		if (tag == "dungeonCard") {
			createDungeonHud ();
		}

		if (tag == "dungeonGameOver") {
			createGameOverDungeonHud ();
		}

		if (tag == "dungeonCardLarge") {
			createGameOverLargeDungeonHud ();
		}

	}

	void Update () {
		if ((dungeonClicked) && (!isShowingCardCollection)) {
			roomCollectionDistCovered = (Time.time - roomCollectionTranStartTime) * roomCollectionTransitionSpeed;
			roomShowCollectionFracJorney = roomCollectionDistCovered / roomCollectionJorneyLength;
			dungeonCollectionSprite.transform.position = Vector2.Lerp (dungeonCollectionSprite.transform.position, 
				gameBackground.transform.position, roomShowCollectionFracJorney);
			if (dungeonCollectionSprite.transform.position == gameBackground.transform.position) {
				dungeonClicked = false;
				isShowingCardCollection = true;
			}
		}
		
		if (((dungeonClicked) && (isShowingCardCollection)) || forceHideCardCollection) {
			roomCollectionDistCovered = (Time.time - roomCollectionTranStartTime) * (roomCollectionTransitionSpeed + 20);
			roomHideCollectionFrecJorney = roomCollectionDistCovered / roomCollectionJorneyLength;
			dungeonCollectionSprite.transform.position = Vector2.Lerp (gameBackground.transform.position, 
				dungeonCollectionOriginalPosition, roomHideCollectionFrecJorney);
			if (dungeonCollectionSprite.transform.position == dungeonCollectionOriginalPosition) {
				isShowingCardCollection = false;
				dungeonClicked = false;
				forceHideCardCollection = false;
			}
		}
	}

	public void createGameOverLargeDungeonHud() {

		GameObject clone = Instantiate(dungeonHud) as GameObject;

		clone.transform.position = this.transform.position;
		clone.name = this.name+"_hud";
		clone.transform.parent = transform; 
		clone.transform.position = new Vector2 (clone.transform.position.x - 0.09f, clone.transform.position.y - 0.1f);
		clone.GetComponentsInChildren<Canvas> ()[0].sortingLayerName = "GameOverCardsLayer";

		Text[] hudTexts =  clone.GetComponentsInChildren<Text> ();

		Image[] hudImages = clone.GetComponentsInChildren<Image> ();

		for (int j = 0; j < hudImages.Length; j++) {
			Destroy (hudImages [j]);
		}


		for (int i = 0; i < hudTexts.Length; i++) {
			
			if (hudTexts [i].name == "priceSimbol") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "damageValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "lifeValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "defenceChanceValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "priceValue") {
				Destroy(hudTexts[i]);
			}

			if (hudTexts [i].name == "upgradeLVLValue") {
				hudTexts [i].text = completeWithZeroes (roomLevel);
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "upgradePointsValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "nameValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "largeNameValue") {
				//hudTexts [i].text = roomName;
				Destroy (hudTexts [i]);
			}
		}
	}

	public void createGameOverDungeonHud() {
	
		GameObject clone = Instantiate(dungeonHud) as GameObject;

		clone.transform.position = this.transform.position;
		clone.name = this.name+"_hud";
		clone.transform.parent = transform; 
		clone.transform.position = new Vector2 (clone.transform.position.x - 0.09f, clone.transform.position.y - 0.1f);
		clone.GetComponentsInChildren<Canvas> ()[0].sortingLayerName = "GameOverCardsLayer";

		Text[] hudTexts =  clone.GetComponentsInChildren<Text> ();

		Image[] hudImages = clone.GetComponentsInChildren<Image> ();

		for (int j = 0; j < hudImages.Length; j++) {
			Destroy (hudImages [j]);
		}

		for (int i = 0; i < hudTexts.Length; i++) {

			if (hudTexts [i].name == "priceSimbol") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "damageValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "lifeValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "defenceChanceValue") {
				Destroy (hudTexts [i]);
			}
				
			if (hudTexts [i].name == "priceValue") {
				Destroy(hudTexts[i]);
			}

			if (hudTexts [i].name == "largeName") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "largeNameValue") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "nameValue") {
				hudTexts [i].text = roomName;
			}
		}
	}

	public void createDungeonHud() {
		GameObject clone = Instantiate(dungeonHud) as GameObject;
		clone.transform.position = this.transform.position;
		clone.name = this.name+"_hud";
		clone.transform.parent = transform; 
		clone.transform.position = new Vector2 (clone.transform.position.x - 0.09f, clone.transform.position.y - 0.1f);
		Text[] hudTexts =  clone.GetComponentsInChildren<Text> ();

		for (int i = 0; i < hudTexts.Length; i++) {

			if (hudTexts [i].name == "priceValue") {
				hudTexts [i].text = completeWithZeroes(dungeonGoldPrice);
			}

			if (hudTexts [i].name == "damageValue") {
				hudTexts [i].text = completeWithZeroes (dungeonRoomDamage);
			}

			if (hudTexts [i].name == "lifeValue") {
				hudTexts [i].text = completeWithZeroes (dungeonLife);
			}

			if (hudTexts [i].name == "defenceChanceValue") {
				hudTexts [i].text = "%"+completeWithZeroes(dungeonDefenceChance);
			}

			if (hudTexts [i].name == "nameValue") {
				hudTexts [i].text = "";
			}

			if (hudTexts [i].name == "largeName") {
				Destroy (hudTexts [i]);
			}

			if (hudTexts [i].name == "largeNameValue") {
				Destroy (hudTexts [i]);
			}
		}
	}

	private string completeWithZeroes(int value) {
		if (value < 10) {
			return "0" + value.ToString ();	
		} else {
			return value.ToString();	
		}
	}

	public static void setRayCasterBitmapToDungeonRoom() {
		Physics2DRaycaster rayCaster = Camera.main.GetComponent<Physics2DRaycaster>();
		rayCaster.eventMask = DUNGEON_CARD_EVENT_MASK;
	}

	public void OnPointerClick(PointerEventData pointerEventData) {
		DungeonGameManager dungeonGameManager = GameObject.Find ("GameManager").GetComponent<DungeonGameManager> ();
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playTapRoom ();
		if ((!dungeonClicked) && 
			(!isShowingCardCollection) && 
			(tag == "dungeonRoom") && 
			(dungeonID != "dungeonRoom_BOSS" )  && 
			(dungeonID != "dungeonRoom_PORTALVERTICAL" ) && 
			(dungeonID != "dungeonRoom_TREASURE" )) {

			Physics2DRaycaster rayCaster = Camera.main.GetComponent<Physics2DRaycaster>();
			rayCaster.eventMask = DUNGEON_ROOM_EVENT_MASK;
			dungeonClicked = true;
			roomCollectionTranStartTime = Time.time;
			roomCollectionJorneyLength = Vector2.Distance (dungeonCollectionSprite.transform.position, 
				gameBackground.transform.position);
			dungeonGameManager.roomToChange = name;
		}

		if ((!dungeonClicked) && (isShowingCardCollection) && (tag == "dungeonCard")){
			Physics2DRaycaster rayCaster = Camera.main.GetComponent<Physics2DRaycaster>();
			rayCaster.eventMask = DUNGEON_CARD_EVENT_MASK;
			dungeonClicked = true;
			roomCollectionTranStartTime = Time.time;
			roomCollectionJorneyLength = Vector2.Distance (dungeonCollectionSprite.transform.position, 
				dungeonCollectionOriginalPosition);
			GameObject roomTochangeGameObject = dungeonGameManager.searchRoomCollectionByName (dungeonID);

            //check player balace before change castle room and if there is no balance does not change it
            //show some message or play sound effect
            
			if (dungeonGameManager.getTotalGold() >= dungeonGoldPrice) {
				dungeonGameManager.changeCastleRoom (roomTochangeGameObject, dungeonGameManager.roomToChange, this.monsterClass);
				dungeonGameManager.updateHeroGold (-dungeonGoldPrice);
				dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playTapRoom ();
			} else {
				dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playNoGoldToBuyRoom ();
			}
		}
	}


	public void roomIdle() {
		if (dungeonRoomAnimator == null) {
			return;
		}
		dungeonRoomAnimator.SetBool ("dungeonRoomDefendBoolean", false);
		dungeonRoomAnimator.SetBool ("dungeonRoomHitBoolean", false);	
	}
		
	public int roomAttack() {
		if (dungeonRoomAnimator == null) {
			return 0;
		}
		dungeonRoomAnimator.SetBool ("dungeonRoomDefendBoolean", false);
		dungeonRoomAnimator.SetBool ("dungeonRoomHitBoolean", false);
		dungeonRoomAnimator.SetTrigger("dungeonRoomAttack");
		if (dungeonID == "dungeonRoom_BOSS") {
			dungeonSoundManager.playBossAttack ();
		} else {
			dungeonSoundManager.playDungeonAttack ();
		}
		return dungeonRoomDamage;
	}

	public void roomDefend(int heroDamagePoints, string roomCastleID) {
		if (dungeonRoomAnimator == null) {
			return;
		}

		if (dungeonID == "dungeonRoom_BOSS") {
			DungeonGameHud dungeonGameHud = GameObject.Find ("GameHud").GetComponent<DungeonGameHud> ();
			dungeonGameHud.updateBossMeter (dungeonLife, dungeonRoomArmor);
		}
			
		if (dungeonLife <= 0) {
			dungeonRoomAnimator.SetBool ("dungeonRoomDie", true);
			isDungeonRoomDead = true;
			dungeonRoomAnimator.SetBool ("dungeonRoomDefendBoolean", false);
			dungeonRoomAnimator.SetBool ("dungeonRoomHitBoolean", false);
			
			Invoke ("roomDestroy",3);

			if (dungeonID == "dungeonRoom_BOSS") {
				DungeonGameManager dungeonGameManager = GameObject.Find ("GameManager").GetComponent<DungeonGameManager> ();
				dungeonGameManager.gameState = DungeonGameManager.GameState.GAME_OVER;
				dungeonSoundManager.playBossDie ();
			} else {
				dungeonSoundManager.playDungeonDie ();
			}
				
		} else {
			int randomValue = Random.Range (0, 100);
			if (randomValue < dungeonDefenceChance) {
				dungeonRoomAnimator.SetBool ("dungeonRoomHitBoolean", false);
				dungeonRoomAnimator.SetBool ("dungeonRoomDefendBoolean", true);
				dungeonSoundManager.playDungeonDefence ();	
			} else {
				dungeonRoomAnimator.SetBool ("dungeonRoomDefendBoolean", false);
				dungeonRoomAnimator.SetBool ("dungeonRoomHitBoolean", true);
				dungeonLife = dungeonLife - heroDamagePoints;
			}
		}
	}

	void roomDestroy() {
		GameObject clone = Instantiate(dungeonDefaultRoom) as GameObject;
		clone.transform.position = this.transform.position;
		clone.name = this.name;
		GameObject depletedRoom = GameObject.Find (clone.name);
		Destroy (depletedRoom);
	}

	/// <summary>
	/// This method sets the respective animation controller for the already stored prefab of the dungeonroom, this
	/// selection needs to be based on the dungeon room class "Small", "Medium" or "Large"
	/// </summary>
	private void setupAnimatorController() {
		AnimationClipOverrides clipOverrides;

		//this can be fixed as the roomSprites is only used to fetch the number of the sprites, it is fixed as 5
		Sprite[] roomSprites = Resources.LoadAll<Sprite> ("Sprites\\Rooms\\Room"+dungeonNumber);

		Animator dungeonRoomAnimator = GetComponent<Animator> ();
		AnimatorOverrideController dungeonRoomOverrideAnimator = 
			new AnimatorOverrideController (dungeonRoomAnimator.runtimeAnimatorController);

		clipOverrides = new AnimationClipOverrides (dungeonRoomOverrideAnimator.overridesCount);
		dungeonRoomOverrideAnimator.GetOverrides (clipOverrides);

		for (int j = 0; j < (roomSprites.Length); j++) {
			
			string filename = "";

			switch (j) {

			case 0:
				filename = dungeonNumber+"\\"+monsterClass+"DungeonRoom"+dungeonNumber+"Attack";
				clipOverrides ["DungeonRoom1Attack"] = 
					Resources.Load<AnimationClip> ("Animations\\Animations\\dugeonRoomAnimation\\DungeonRoom"+filename);
				break;
			case 1:
				filename = dungeonNumber+"\\"+monsterClass+"DungeonRoom"+dungeonNumber+"Die";
				clipOverrides["DungeonRoom1Die"] = 
					Resources.Load<AnimationClip> ("Animations\\Animations\\dugeonRoomAnimation\\DungeonRoom"+filename);
				break;
			case 2:
				filename = dungeonNumber+"\\"+monsterClass+"DungeonRoom" +dungeonNumber+"Defend";
				clipOverrides["DungeonRoom1Defend"] = 
					Resources.Load<AnimationClip> ("Animations\\Animations\\dugeonRoomAnimation\\DungeonRoom"+filename);
				break;
			case 3: 
				filename = dungeonNumber+"\\"+monsterClass+"DungeonRoom"+dungeonNumber+"Hit";
				clipOverrides["DungeonRoom1Hit"] = 
					Resources.Load<AnimationClip> ("Animations\\Animations\\dugeonRoomAnimation\\DungeonRoom"+filename);
				
				break;
			case 4:
				filename = dungeonNumber+"\\"+monsterClass+"DungeonRoom" +dungeonNumber+"Idle";
				clipOverrides["DungeonRoom1Idle"] = 
					Resources.Load<AnimationClip> ("Animations\\Animations\\dugeonRoomAnimation\\DungeonRoom"+filename);
				break;
			}

		}

	
		dungeonRoomOverrideAnimator.ApplyOverrides (clipOverrides);
		dungeonRoomAnimator.runtimeAnimatorController = dungeonRoomOverrideAnimator;

	
	}
}

public class AnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
{
	public AnimationClipOverrides(int capacity) : base(capacity) {
		
	}

	public AnimationClip this[string name]
	{
		get { 
			return this.Find(x => x.Key.name.Equals(name)).Value; 
		}
		set
		{
			int index = this.FindIndex(x => x.Key.name.Equals(name));
			if (index != -1)
				this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
		}
	}
}
