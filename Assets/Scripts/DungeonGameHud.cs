﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DungeonGameHud : MonoBehaviour {
	public Text goldCanvasText;
	public Text daysLeftCanvasText;
	public Text bossMeterText;
	public Text earnedGoldText;
	public Text daysLeft;

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showHud() {
		var iTweenLabelsHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",2.0f, "y",9.0f);
		var iTweenValuesHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",2.0f, "y",8.3f);
		iTween.MoveTo (earnedGoldText.gameObject, iTweenLabelsHash);
		iTween.MoveTo (daysLeftCanvasText.gameObject, iTweenLabelsHash);
		iTween.MoveTo (daysLeft.gameObject, iTweenValuesHash);
		iTween.MoveTo (goldCanvasText.gameObject, iTweenValuesHash);
		Invoke("createBossHearts", 0.5f);
	}

	public void hideHud() {
		var iTweenLabelsHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",2.0f, "y",3630.0f);
		var iTweenValuesHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",2.0f, "y",3500.0f);
		iTween.MoveTo (bossMeterText.gameObject, iTweenLabelsHash);
		iTween.MoveTo (earnedGoldText.gameObject, iTweenLabelsHash);
		iTween.MoveTo (daysLeftCanvasText.gameObject, iTweenLabelsHash);
		iTween.MoveTo (daysLeft.gameObject, iTweenValuesHash);
		iTween.MoveTo (goldCanvasText.gameObject, iTweenValuesHash);
	}

	IEnumerator waitToAnimate(int i) {
		Vector3 heartScale = new Vector3 (0.75f, 0.75f, 0.0f);
		yield return new WaitForSeconds ((float)i/5.0f);
		GameObject heart = GameObject.Find ("Heart"+i);
		var iTweenHeartScaleHash = iTween.Hash ("easetype", iTween.EaseType.easeOutElastic, "time",1.0f,"scale",heartScale);
		iTween.ScaleTo(heart, iTweenHeartScaleHash);
	}

	public void createBossHearts() {
		for (int i = 1; i <= 10 ; i++) {
			StartCoroutine (waitToAnimate (i));
		}
	}

	public void updateHudGold(int gold) {
	    Text goldValue = GameObject.Find("GoldValue").GetComponent<Text>();
	    goldValue.text = gold.ToString();
	}

	public void updateBossMeter(float bossLife, float bossArmor) {
		float heartLifeValue = bossArmor / 10;
		float damageDelta = bossArmor - bossLife;
		int heartsToDestroy = (int) Mathf.Floor(damageDelta / heartLifeValue); 
		heartsToDestroy = Mathf.Min (heartsToDestroy, 10);
		for (int i = 10; i > (10 - heartsToDestroy); i--) {
			Transform heartTransform = GameObject.Find ("Heart"+i).GetComponent<Transform> ();
			heartTransform.localScale = new Vector2 (0.0f, 0.0f); 
		}
	}

    public void updateRemaingingDays(int days) {
		Text daysLeftValue = GameObject.Find ("DaysLeftValue").GetComponent<Text> ();
		daysLeftValue.text = days.ToString();
	}
}
