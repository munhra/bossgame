﻿using UnityEngine;
using GoogleMobileAds.Api;

public class DungeonGameAds : MonoBehaviour
{
    private string IOS_BANNER_ADS_UNIT_TEST = "ca-app-pub-3940256099942544/2934735716";
    private string ANDROID_BANNER_ADS_UNIT_TEST = "ca-app-pub-3940256099942544/6300978111";

    private string PRODUCTION_IOS_BANNER_ADS_UNIT_TEST = "ca-app-pub-3126160639257364/8386000130";
    private string PRODUCTION_ANDROID_BANNER_ADS_UNIT_TEST = "ca-app-pub-3126160639257364/1699396187";

    private BannerView bannerView;

    // Start is called before the first frame update
    void Start()
    {
        MobileAds.Initialize (initStatus => {
            Debug.Log ("MobileAds.Initialize ");
            requestBanner ();
        });
    }

	private void requestBanner ()
	{
        #if UNITY_ANDROID
            string adUnitId = Debug.isDebugBuild ? ANDROID_BANNER_ADS_UNIT_TEST : PRODUCTION_ANDROID_BANNER_ADS_UNIT_TEST;
        #elif UNITY_IPHONE
            string adUnitId = IOS_BANNER_ADS_UNIT_TEST;
        #else
            string adUnitId = "unexpected_platform";
        #endif

        bannerView = new BannerView (adUnitId, AdSize.Banner, AdPosition.Bottom);
        AdRequest adRequest = new AdRequest.Builder ().Build ();
        bannerView.LoadAd (adRequest);
    }
}
