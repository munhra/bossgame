﻿using System.Collections;
using UnityEngine;

public class DungeonGameSoundManager : MonoBehaviour
{
	private AudioClip buttonClick;
	private AudioClip bossTitleSlidesSFX;
	private AudioClip castleHitGroundSFX;
	private AudioClip castleLaunchSFX;
	private AudioClip dungeonAppearSFX;
	private AudioClip swordAppearsSFX;
	private AudioClip retroTitleAppearSFX;

	private AudioClip tapRoomSFX;
	private AudioClip noGoldToBuyRoom;
	private AudioClip openChest;
	private AudioClip portalGameplay;

	private AudioClip heroDying;
	private AudioClip heroAttack;
	private AudioClip heroDefend;

	private AudioClip dungeonDying;
	private AudioClip dungeonAttack;
	private AudioClip dungeonDefend;

	private AudioClip bossDying;
	private AudioClip bossAttack;

	private AudioClip gameOverScreenMusic;
	private AudioClip titleScreenMusic;
	private AudioClip upgradeScreenMusic;
	private AudioClip endGameScreenMusic;

	private AudioClip [] levelMusic = new AudioClip [6];

	public bool disableMusic = false;

	private AudioSource audioSource;
	private AudioSource musicAudioSource;

	public GameObject dungeonGameManagerGameObject;
	private DungeonGameManager dungeonGameManager;

	// Start is called before the first frame update
	void Start()
    {
		// Title Screen
		buttonClick = Resources.Load<AudioClip> ("Sounds/DungeonButtonClick");
		bossTitleSlidesSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/bossTitleSlides");
		castleHitGroundSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/castleHitGround");
		castleLaunchSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/castleLaunch");
		dungeonAppearSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/dungeonAppears");
		swordAppearsSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/swordAppears");
		retroTitleAppearSFX = Resources.Load<AudioClip> ("Sounds/titleScreen/retroTitleAppears");

		// Game PLay
		tapRoomSFX = Resources.Load<AudioClip> ("Sounds/dungeonSelectionScreen/tapRoom2");
		noGoldToBuyRoom = Resources.Load<AudioClip> ("Sounds/dungeonSelectionScreen/noGoldToBuyRoom");
		openChest = Resources.Load<AudioClip> ("Sounds/gamePlayScreen/openChest");
		portalGameplay = Resources.Load<AudioClip> ("Sounds/gamePlayScreen/portalGameplay");

		// Hero Sounds
		heroDying = Resources.Load<AudioClip> ("Sounds/heroSounds/enemyDying");
		heroAttack = Resources.Load<AudioClip> ("Sounds/heroSounds/enemyAttack");
		heroDefend = Resources.Load<AudioClip> ("Sounds/heroSounds/enemyBlock");

		// Dungeon Sounds
		dungeonDying = Resources.Load<AudioClip> ("Sounds/dungeonSounds/dungeonDie");
		dungeonAttack = Resources.Load<AudioClip> ("Sounds/dungeonSounds/dungeonAttack3");
		dungeonDefend = Resources.Load<AudioClip> ("Sounds/dungeonSounds/dungeonBlock");

		// Boss sounds
		bossDying = Resources.Load<AudioClip> ("Sounds/bossSounds/bossDie");
		bossAttack = Resources.Load<AudioClip> ("Sounds/bossSounds/bossAttack");

		// Music
		gameOverScreenMusic = Resources.Load<AudioClip> ("Sounds/music/gameOverScreen");
		titleScreenMusic = Resources.Load<AudioClip> ("Sounds/music/titleScreen");
		upgradeScreenMusic = Resources.Load<AudioClip> ("Sounds/music/upgradeScreen");
		endGameScreenMusic = Resources.Load<AudioClip> ("Sounds/music/endGameMusic");

		loadLevelMusic ();

		AudioSource[] audioSourceList = GetComponents<AudioSource> ();
		audioSource = audioSourceList [0];
		musicAudioSource = audioSourceList [1];
		dungeonGameManager = dungeonGameManagerGameObject.GetComponent<DungeonGameManager> ();
	}

	private void Update ()
	{
		if (dungeonGameManager.gameState == DungeonGameManager.GameState.GAME_STARTED &&
			!musicAudioSource.isPlaying) {
			playRandomLevelMusic ();
		}
	}

	private void loadLevelMusic ()
	{
		for (int i = 1; i <= 6; i++) {
			levelMusic[i - 1] = Resources.Load<AudioClip> ("Sounds/music/level"+i.ToString());
		}
		
	}

	public void playRandomLevelMusic()
	{
		if (!disableMusic) {
			int randomMusicIndex = Random.Range (0, 6);
			musicAudioSource.PlayOneShot (levelMusic [randomMusicIndex]);
		}
	}

	public void playBossTitleSlides()
	{
		audioSource.PlayOneShot (bossTitleSlidesSFX);
	}

	public void playcastleHitGround()
	{
		audioSource.PlayOneShot (castleHitGroundSFX);
	}

	public void playCastleLaunch()
	{
		audioSource.PlayOneShot (castleLaunchSFX);
	}

	IEnumerator PlayDelayedSound(float seconds, AudioClip audioClip)
	{
		yield return new WaitForSeconds (seconds);
		audioSource.PlayOneShot (audioClip);
	}

	public void playDungeonAppearSFX()
	{
		StartCoroutine (PlayDelayedSound (1, dungeonAppearSFX));
	}

	public void playSwordAppearsSFX()
	{
		StartCoroutine (PlayDelayedSound (0.7f, swordAppearsSFX));
	}

	public void playRetroTitleAppearSFX()
	{
		StartCoroutine (PlayDelayedSound (0.2f, retroTitleAppearSFX));
	}

	public void playButtonClick()
	{
		audioSource.PlayOneShot (buttonClick);
	}

	public void playTapRoom()
	{
		audioSource.PlayOneShot (tapRoomSFX);
	}

	public void playNoGoldToBuyRoom()
	{
		audioSource.PlayOneShot (noGoldToBuyRoom);
	}

	public void playHeroDying ()
	{
		audioSource.PlayOneShot (heroDying);
	}

	public void playHeroAttack()
	{
		audioSource.PlayOneShot (heroAttack);
	}

	public void playHeroDefend()
	{
		audioSource.PlayOneShot (heroDefend);
	}

	public void playDungeonDie()
	{
		audioSource.PlayOneShot (dungeonDying);
	}

	public void playDungeonAttack()
	{
		audioSource.PlayOneShot (dungeonAttack);
	}

	public void playDungeonDefence()
	{
		audioSource.PlayOneShot (dungeonDefend);
	}

	public void playBossAttack ()
	{
		audioSource.PlayOneShot (bossAttack);
	}

	public void playBossDie ()
	{
		audioSource.PlayOneShot (bossDying);
	}

	public void playOpenChest ()
	{
		audioSource.PlayOneShot (openChest);
	}

	public void playPortalGameplay ()
	{
		audioSource.PlayOneShot (portalGameplay);
	}

	public void playTitleScreenMusic()
	{
		if (!disableMusic) {
			musicAudioSource.PlayOneShot (titleScreenMusic);
		}
	}

	public void playUpgradeScreenMusic ()
	{
		musicAudioSource.PlayOneShot (upgradeScreenMusic);
	}

	public void playGameOverMusic ()
	{
		musicAudioSource.PlayOneShot (gameOverScreenMusic);
	}

	public void playEndGameMusic ()
	{
		musicAudioSource.PlayOneShot (endGameScreenMusic);
	}

	public void stopAudio()
	{
		audioSource.Stop ();
		musicAudioSource.Stop ();
	}
}
