﻿using UnityEngine;
using UnityEngine.UI;

public class DungeonLogo : MonoBehaviour {
	public GameObject logoSword;
	public GameObject logoBoss;
	public GameObject logoFront;
	public GameObject logoRetro;
	public GameObject innerLogoBoss;
	public GameObject outterLogoBoss;
	public GameObject dungeonLogoFrontBack;

	public GameObject letterR2;
	public GameObject letterE;
	public GameObject letterR1;
	public GameObject letterO;

	public GameObject logoBright1;
	public GameObject logoBright2;
	public GameObject logoBright3;
	public GameObject logoBright4;

	public GameObject logoCastleWall;
	public Button playButton;
	public bool disableLog = false;
	bool rightRetroAnim = true;

	public GameObject dungeonSoundManager;

	void Start () {
		if (!disableLog) {
			Invoke ("moveOutlogoCastleWall", 1.0f);
			Invoke ("moveButton", 6.0f);
			Invoke ("moveInlogoCastleWall", 3.0f);
			Invoke ("moveLogo", 4.0f);
			Invoke ("animateRetroLogo", 7.0f);
			Invoke ("colorLogo", 8.0f);
			Invoke ("showDungeonLogoFrontBack", 5.4f);
			InvokeRepeating ("animateAllLetters", 0.0f, 3.5f);
		}
	}
		
	void Update () {
		
	}

	public void stopAnimations()
	{
		iTween.Stop ();
	}
		
	public void moveButton() {
	    var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",0.5f, "x",0.45f);
	    iTween.MoveTo (playButton.gameObject, iTweenLogoHash);
	}

    public void moveOutButton()
    {
        var iTweenButtonHash = iTween.Hash("easetype", iTween.EaseType.easeOutBack, "time", 0.5f, "x", -8.5f);
        iTween.MoveTo(playButton.gameObject, iTweenButtonHash);
    }
		
	void showDungeonLogoFrontBack () {
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 255.0f);
		iTween.FadeTo (dungeonLogoFrontBack, iTweenLogoHash);
		dungeonLogoFrontBack.GetComponent<Renderer> ().enabled = true;
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playDungeonAppearSFX ();
	}

	public void dismissLogo() {
		stopAnimations ();

		CancelInvoke ("moveLogoSword");
		CancelInvoke ("animateAllLetters");
		CancelInvoke ("animateRetroLogo");

		//Dismiss Sword
		var iTweenSwordHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",1.0f, "y", 15.74f);
		iTween.MoveTo (logoSword, iTweenSwordHash);

		var iTweenCastleWallHash = iTween.Hash ("easetype", iTween.EaseType.easeInExpo, "time",1.0f, "y", 17.00f);
		iTween.MoveTo (logoCastleWall, iTweenCastleWallHash);

		Vector3 punchPower = new Vector3 (2.0f, 2.0f, 0.0f);
		var iTweenRetroHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",0.5f, "alpha", 0.00f);
		iTween.FadeTo(logoRetro,iTweenRetroHash);
		iTween.PunchScale (logoRetro, punchPower, 3.0f);

		Vector3 bossScaleVector = new Vector3 (8.0f, 8.0f, 0.0f);
		var iTweenBOSSHashScale = iTween.Hash ("easetype", iTween.EaseType.easeOutElastic, "time",2.0f,"scale",bossScaleVector);
		var iTweenBossHashFade = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",2.0f, "alpha", 0.00f);
		iTween.FadeTo (logoBoss, iTweenBossHashFade);
		iTween.ScaleTo (logoBoss, iTweenBOSSHashScale);

		var iTweenBackDungeonHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 0.00f);
		iTween.FadeTo (dungeonLogoFrontBack, iTweenBackDungeonHash);
		iTween.FadeTo (logoFront, iTweenBackDungeonHash);
		moveOutButton ();
	}

	public void moveOutlogoCastleWall() {
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeInExpo, "time",1.0f, "y", 15.79f);
		iTween.MoveTo (logoCastleWall, iTweenLogoHash);
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playCastleLaunch ();
	}

	public void moveInlogoCastleWall() {
		logoCastleWall.transform.position = new Vector3 (0.05f, 15.79f, 0.0f);
		logoCastleWall.transform.localScale = new Vector3 (1.0f, 1.0f, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBounce, "time",1.0f, "y", 0.45f, "x", 0.05f);
		iTween.MoveTo (logoCastleWall, iTweenLogoHash);
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playcastleHitGround ();
	}


	public void animateAllLetters() {
		Invoke ("animateRetroLettersR1", 6.0f);
		Invoke ("animateRetroLettersE", 6.5f);
		Invoke ("animateRetroLettersR2", 7.0f);
		Invoke ("animateRetroLettersO", 7.5f);
		//dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playRetroTitleAppearSFX ();
	}

	public void animateRetroLettersR1() {
		float scaleFactorSingleValue = rightRetroAnim ? 1.3f : 1.0f;
		Vector3 scaleFactor = new Vector3 (scaleFactorSingleValue,scaleFactorSingleValue, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutCirc,"scale", scaleFactor, "time",0.5f, "looptype",iTween.LoopType.none );
		iTween.ScaleTo(letterR1, iTweenLogoHash);
		float fadeFactor = rightRetroAnim ? 255.0f : 0.0f;
		var iTweenBrightHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", fadeFactor);
		iTween.FadeTo (logoBright1, iTweenBrightHash);
	}

	public void animateRetroLettersE() {
		float scaleFactorSingleValue = rightRetroAnim ? 1.3f : 1.0f;
		Vector3 scaleFactor = new Vector3 (scaleFactorSingleValue,scaleFactorSingleValue, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutExpo,"scale", scaleFactor, "time",0.5f, "looptype",iTween.LoopType.none );
		iTween.ScaleTo(letterE, iTweenLogoHash);
		float fadeFactor = rightRetroAnim ? 255.0f : 0.0f;
		var iTweenBrightHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", fadeFactor);
		iTween.FadeTo (logoBright2, iTweenBrightHash);
	}

	public void animateRetroLettersR2() {
		float scaleFactorSingleValue = rightRetroAnim ? 1.3f : 1.0f;
		Vector3 scaleFactor = new Vector3 (scaleFactorSingleValue,scaleFactorSingleValue, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutExpo,"scale", scaleFactor, "time",0.5f, "looptype",iTween.LoopType.none );
		iTween.ScaleTo(letterR2, iTweenLogoHash);
		float fadeFactor = rightRetroAnim ? 255.0f : 0.0f;
		var iTweenBrightHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", fadeFactor);
		iTween.FadeTo (logoBright3, iTweenBrightHash);
	}

	public void animateRetroLettersO() {
		float scaleFactorSingleValue = rightRetroAnim ? 1.3f : 1.0f;
		Vector3 scaleFactor = new Vector3 (scaleFactorSingleValue,scaleFactorSingleValue, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutExpo,"scale", scaleFactor, "time",0.5f, "looptype",iTween.LoopType.none );
		iTween.ScaleTo(letterO, iTweenLogoHash);
		rightRetroAnim = !rightRetroAnim;
		float fadeFactor = rightRetroAnim ? 255.0f : 0.0f;
		var iTweenBrightHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", fadeFactor);
		iTween.FadeTo (logoBright4, iTweenBrightHash);
	}
		
	public void animateRetroLogo() {
		Vector3 punchPower = new Vector3 (2.0f, 2.0f, 0.0f);
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 255.0f);
		iTween.FadeTo(logoRetro,iTweenLogoHash);
		iTween.PunchScale (logoRetro, punchPower, 3.0f);
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playRetroTitleAppearSFX ();
	}

	public void moveLogoSword() {
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",1.0f, "y", 2.65f);
		iTween.MoveTo (logoSword, iTweenLogoHash);
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playSwordAppearsSFX ();
	}

	void shakeLogo() {
		Vector3 punchPower = new Vector3 (0.01f, 0.01f, 0.0f);
		var iTweenLogoHash = iTween.Hash ("amount", punchPower, "time",3.0f, "looptype",iTween.LoopType.pingPong );
		iTween.ShakeScale(logoBoss, iTweenLogoHash);
	}

	void colorLogo() {
		Color color = new Color (2, 0, 0);
		var iTweenColorHash = iTween.Hash ("easetype", iTween.EaseType.easeInExpo,"color", color, "time", 1.0f, 
			"loopType", iTween.LoopType.pingPong);
		iTween.ColorTo (innerLogoBoss,iTweenColorHash);
	}
		
	void moveLogo() {
		var iTweenBOSSHash = iTween.Hash ("easetype", iTween.EaseType.easeOutElastic, 
			"time",2.0f, "x", 1.25f, "oncomplete", "moveDungeonLogoFront","oncompletetarget",logoFront);
		Invoke ("moveLogoSword", 4.0f);
		Invoke ("shakeLogo", 4.5f);
		iTween.MoveTo (logoBoss, iTweenBOSSHash);
		dungeonSoundManager.GetComponent<DungeonGameSoundManager>().playBossTitleSlides ();
	}
}
