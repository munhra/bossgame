﻿using UnityEngine;

public class DungeonCollection : MonoBehaviour
{
	private GameObject [] dungeonRoomArray;
	public Vector3 initialPosition;
	private float roomSpaceX = 1.40F;
	private float roomSpaceY = 0.88F;

	public void updateRoomCardCollectionAtPosition (int cardPosition)
	{
		GameObject loadedDungeonRoom = Resources.Load<GameObject> (DungeonGameManager.earnedRoomMonsterClass [cardPosition]);
		dungeonRoomArray [cardPosition] = loadedDungeonRoom;
		int colCount = -1;
		int rowCount = -1;

		for (int i = 0; i <= cardPosition; i++) {
			if (i % 3 == 0) {
				rowCount++;
				colCount = 0;
			} else {
				colCount++;
			}
		}

		GameObject dungeonCardToReplace = GameObject.Find ("collectionRoom_"+cardPosition);

		Destroy (dungeonCardToReplace);
		GameObject clone;
		GameObject dungeonRoom = (GameObject)dungeonRoomArray [0];
		BoxCollider2D box2DCollider = dungeonRoom.GetComponent<BoxCollider2D> ();

		float roomWidth = box2DCollider.size.x;
		float roomHeight = box2DCollider.size.y;
		clone = Instantiate (dungeonRoomArray [cardPosition]) as GameObject;

		clone.tag = "dungeonCard";
		clone.layer = 11;
		clone.transform.parent = this.transform;
		clone.transform.position = new Vector3 (initialPosition.x + this.transform.position.x + (colCount * roomWidth * roomSpaceX),
			initialPosition.y + this.transform.position.y - (rowCount * roomHeight * roomSpaceY));
		
		clone.GetComponent<SpriteRenderer> ().sortingLayerName = "DungeonCollectionCards";
		clone.name = "collectionRoom_" + cardPosition;
		clone.GetComponent < DungeonRoom > ().dungeonCreatedFromUpdate = true;
	}

	/// <summary>
	/// This method builds the card collection that the user can select from its collection
	/// </summary>
	public void buildRoomCardCollection ()
	{
		dungeonRoomArray = new GameObject [18];
		for (int i = 0; i < 18; i++) {
			if (DungeonGameManager.earnedRoomMonsterClass[i] != DungeonRoom.ROOM_CLASS_LOCKED) {
				GameObject loadedDungeonRoom = Resources.Load<GameObject> (DungeonGameManager.earnedRoomMonsterClass[i]);
				dungeonRoomArray [i] = loadedDungeonRoom;
			}
		}
		GameObject dungeonRoom = (GameObject)dungeonRoomArray [0];
		BoxCollider2D box2DCollider = dungeonRoom.GetComponent<BoxCollider2D> ();

		float roomWidth = box2DCollider.size.x;
		float roomHeight = box2DCollider.size.y;

		int colCount = 0;
		int rowCount = -1;
	
		for (int i = 0; i < dungeonRoomArray.Length; i++) {

			if (dungeonRoomArray[i] == null) {
				continue;
			}

			GameObject clone;

			if (i % 3 == 0) {
				rowCount++;
				colCount = 0;
			}

			clone = Instantiate (dungeonRoomArray [i]) as GameObject;
			clone.tag = "dungeonCard";
			clone.layer = 11;
			clone.transform.parent = this.transform;
			clone.transform.position = new Vector3 (initialPosition.x + this.transform.position.x + (colCount * roomWidth * roomSpaceX), 
				initialPosition.y + this.transform.position.y - (rowCount * roomHeight * roomSpaceY));
			clone.GetComponent<SpriteRenderer> ().sortingLayerName = "DungeonCollectionCards";
            clone.name = "collectionRoom_" + i;
			colCount++;
        }
	}
}
