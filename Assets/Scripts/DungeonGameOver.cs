﻿using UnityEngine;
using UnityEngine.UI;

public class DungeonGameOver : MonoBehaviour {
	private DungeonGameSoundManager dungeonSoundManager;
	public Button playAgainGameButton;
	public GameObject bossGrave;
	public GameObject bossDead;
	public GameObject bossWin;
	public GameObject gameOverTitle;

	void Start ()
	{
		playAgainGameButton.onClick.AddListener (playAgainGameButtonClick);
	}

	public void configureGameOver (bool isLastLevel, int totalGoldEarned, int totalKilledHeroes)
	{
		Text [] gameOverTexts = this.GetComponentsInChildren<Text> ();
		bossGrave.SetActive (!isLastLevel);
		bossDead.SetActive (!isLastLevel);
		bossWin.SetActive (isLastLevel);
		gameOverTitle.SetActive (!isLastLevel);
		for (int i = 0; i < gameOverTexts.Length; i++) {
			if (gameOverTexts [i].name == "totalGoldEarnedValue") {
				gameOverTexts [i].text = completeWithZeroes (totalGoldEarned);
			}
			if (gameOverTexts [i].name == "totalHeroesKilledValue") {
				gameOverTexts [i].text = completeWithZeroes (totalKilledHeroes);
			}
			if (gameOverTexts [i].name == "endGameText" && !isLastLevel) {
				gameOverTexts [i].text = "TRY AGAIN !";
			}
		}
	}      

	private void playAgainGameButtonClick()
	{
		DungeonGameManager dungeonGameManager = GameObject.Find ("GameManager").GetComponent<DungeonGameManager> ();
		dungeonSoundManager = GameObject.Find ("dungeonSoundManager").GetComponent<DungeonGameSoundManager> ();
		if (dungeonGameManager.gameState == DungeonGameManager.GameState.GAME_OVER_SUMMARY) {
			dungeonGameManager.performGameResumeAfterGameOver ();
		}
		dungeonSoundManager.stopAudio ();
		dungeonSoundManager.GetComponent<DungeonGameSoundManager> ().playButtonClick ();
		dungeonSoundManager.playRandomLevelMusic ();
	}

	private string completeWithZeroes (int value)
	{
		if (value < 10) {
			return "0" + value.ToString ();
		} else {
			return value.ToString ();
		}
	}
}
