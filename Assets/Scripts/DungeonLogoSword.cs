﻿using UnityEngine;

public class DungeonLogoSword : MonoBehaviour {
	public void moveLogoSword() {
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time",1.0f, "y", 2.65f);
		iTween.MoveTo (this.gameObject, iTweenLogoHash);
	}
}
