﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using System.Collections;

public class DungeonLevelManager : MonoBehaviour
{
    public int totalWaveNumberOfHeroes;
    public TextAsset levelDataFile;
    public LevelData[] levelDataList;
    public int currentLevel = 1;
    public List<Object> levelHeroesArray = new List<Object>();
    public GameObject dungeonGameData;
    public Object[] allDungeonHeroesArray;
    public Vector3 heroSpanwPosition = new Vector3 (4.3F, -8.94F, 0.0F);

    private void Start()
    {
        if (DungeonGameManager.isClearPlayerPrefs) {
            DungeonGameManager.clearPlayerPrefs ();
        }
        loadDungeonData ();
        loadLevelData();
        loadAllHeroesArray();
        loadLastLevelFromPersistence();
        generateLevelHeroesArray();
    }

    private void loadDungeonData()
	{
        dungeonGameData.GetComponent<DungeonGameData> ().loadDungeonHeroData ();
    }

    public void createLevelHeroArray()
    {
        currentLevel++;
        if (!isLastLevel()) {
            generateLevelHeroesArray ();
            //saveLastLevelToPersistence ();
        } else {
            currentLevel = 1;
            //saveLastLevelToPersistence ();
        }
        saveLastLevelToPersistence ();
        //TODO : Include in the hud the current level, when the player reaches it include an animation
    }

    /// <summary>
    /// This method select from the all heroes array the number of heroes to be added to the level.
    /// </summary>
    public void generateLevelHeroesArray()
    {
        levelHeroesArray.Clear();
        // creates an array ordered by probabiblity of hero based on level
        HeroSelectProbability [] heroSelectProbability = heroSelectionProbVector ();
        Debug.Log ("Current Level Value " + currentLevel);
        int numberOfHeroes = levelDataList [currentLevel].numberOfHeroes;

        // TODO : [HACK NUMBER OF LEVEL HEROES] ---- Remove It
        // numberOfHeroes = 2;

        // after 20 levels 1 hero will be open
        int availableHeroes = Math.Min((currentLevel / 20), allDungeonHeroesArray.Length - 1);
        Debug.Log ("generateLevelHeroesArray availableHeroes " + availableHeroes);
        for (int i = 0; i < numberOfHeroes; i ++)
        {
            int index = Math.Min (availableHeroes, i);
            Debug.Log ("##### generateLevelHeroesArray " + index);
            HeroSelectProbability heroProb = heroSelectProbability [index];
            GameObject clone = Instantiate (allDungeonHeroesArray [heroProb.heroIndex]) as GameObject;
            clone.transform.position = heroSpanwPosition;
            levelHeroesArray.Add(clone);
        }
        resetMarkedToBeRemoved();
        totalWaveNumberOfHeroes += levelHeroesArray.Count;
    }

    private HeroSelectProbability [] heroSelectionProbVector ()
	{
        HeroSelectProbability [] heroSelectProbabilityList = new HeroSelectProbability [allDungeonHeroesArray.Length];
		// normalized chance of the hero be selected 
		float [] heroSelectionChance = new float [allDungeonHeroesArray.Length];
		// Normalize level from 0 to 1
		float normalizedLevel = (float)currentLevel / (float)(levelDataList.Length);
        for (int i = 0; i < allDungeonHeroesArray.Length; i++) {
            HeroSelectProbability heroSelectProbability = new HeroSelectProbability ();
            heroSelectProbability.heroSelectionProb = (float) (i + 1) / (float) allDungeonHeroesArray.Length;
            heroSelectProbability.heroSelectionProb = 1 - Math.Abs (heroSelectProbability.heroSelectionProb - normalizedLevel);
            heroSelectProbability.heroIndex = i;
            heroSelectProbabilityList [i] = heroSelectProbability;
        }
        Array.Sort (heroSelectProbabilityList, new HeroSelectProbabilityComparator ());
		return heroSelectProbabilityList;
    }

    private void resetMarkedToBeRemoved()
    {
        foreach (Object dungeonHeroObject in levelHeroesArray)
        {
            GameObject dungeonHeroGameObject = (GameObject)dungeonHeroObject;
            DungeonHero dungeonHero = dungeonHeroGameObject.GetComponent<DungeonHero>();
            dungeonHero.heroMarkedToBeRemoved = false;
        }
    }

    public void loadLastLevelFromPersistence()
    {
		currentLevel = PlayerPrefs.GetInt ("LastLevelReached");
		if (currentLevel == 0) {
			currentLevel = 1;
		}
        //TODO : LEVEL SELECTION Remove this forced level selection
        //Debug.Log ("loadLastLevelFromPersistence -> " + currentLevel);
        //currentLevel = 299;
	}

    private void saveLastLevelToPersistence()
    {
        PlayerPrefs.SetInt("LastLevelReached", currentLevel);
    }

    private void loadAllHeroesArray()
    {
        allDungeonHeroesArray = Resources.LoadAll("dungeonHeroes", typeof(GameObject)) as Object[];
        Array.Sort (allDungeonHeroesArray, new AllHeroesArrayComparator ());
        dungeonGameData.GetComponent<DungeonGameData> ();
        DungeonGameData data = dungeonGameData.GetComponent<DungeonGameData> ();
        for (int i = 0; i < allDungeonHeroesArray.Length; i++) {
            GameObject dungeonHeroGameObject = allDungeonHeroesArray [i] as GameObject;
            DungeonHero dungeonHero = dungeonHeroGameObject.GetComponent<DungeonHero> ();
            DungeonHeroData heroData = data.dungeonHeroDataDict [dungeonHero.heroName];
            dungeonHero.heroLife = heroData.heroLife;
            dungeonHero.heroGold = heroData.heroGold;
            dungeonHero.heroDefenceChance = heroData.heroDefenceChance;
            dungeonHero.heroSpeed = heroData.heroSpeed;
            dungeonHero.heroTotalDamage = heroData.heroTotalDamage;
            dungeonHero.maxAttackTries = heroData.heroMaxTries;
        }
    }

    private void loadLevelData()
    {
        string dungeonLevelData = levelDataFile.text;
        string[] dataLines = dungeonLevelData.Split(char.Parse("\n"));
        levelDataList = new LevelData[dataLines.Length];
        for (int i = 1; i < dataLines.Length; i++)
        {
            string[] stringLevelData = dataLines[i].Split(char.Parse(","));
            LevelData levelData = new LevelData();
            levelData.numberOfHeroes = Int32.Parse(stringLevelData[1]);
            levelData.isMonsterUpLevel = stringLevelData[2] == "1" ? true : false;
            levelData.totalAccumulatedHeroes = Int32.Parse(stringLevelData[3]);
            levelData.eficiencePercentageToUnlockMonster = Int32.Parse(stringLevelData[4]);
            levelData.accumulatedHeroesPerMonsterWave = Int32.Parse(stringLevelData[5]);
            levelData.minHeroesDeadToUnlockMonster = Int32.Parse(stringLevelData[6]);
            levelData.updateMonsterUpgradeGauge = stringLevelData[7] == "1" ? true : false;
			levelData.prefabDungeonRoomName = stringLevelData [8].Replace("\r","");
			levelDataList [i] = levelData;
        }
    }

    public LevelData getCurrentLevelData()
    {
        return levelDataList[currentLevel];
    }

    public int getMaxLevelNumber()
	{
        return levelDataList.Length;
	}

    public bool isLastLevel()
	{
        return (levelDataList.Length - 1) == currentLevel;
	}
}

/// <summary>
/// This struct is used in hero selection probability logic.
/// </summary>
public struct HeroSelectProbability {
    public float heroSelectionProb;
    public int heroIndex;
}

/// <summary>
/// This struct represents the level data in memory
/// </summary>
public struct LevelData
{
    //number of heroes to be spawned on level
    public int numberOfHeroes;
    //true if the level is a monster upgrade level
    public Boolean isMonsterUpLevel;
    //possible accumulated heroes killed up the level
    public int totalAccumulatedHeroes;
    // discount percentage 0 to 100 of total heroes killed between upgrade
    // monster levels
    public int eficiencePercentageToUnlockMonster;
    // accumulated killed heroes between upgrade levels, this total is zeroed
    // on the next upgrade level
    public int accumulatedHeroesPerMonsterWave;
    // This value is calculated using the percentage stored in
    // "eficiencePercentageToUnlockMonster" of "totalAccumulatedHeroes" on the
    // upgrade level, this factor will be used to balance the difficult of the
    // game
    public int minHeroesDeadToUnlockMonster;
    // flag that indicates when the 3 values of the gauge needs to be updated,
    // this values are the "minHeroesDeadToUnlockMonster" for the next 3 levels
    // where the upgrade will occurr
    public Boolean updateMonsterUpgradeGauge;
	// holds the prefab name of the dungeon when updateMonsterUpgradeGauge is true
	// otherwise it will be empty string ""
	public String prefabDungeonRoomName;
}

/// <summary>
/// This class implements an IComparer to sort in ascending order the array of heroes
/// </summary>
public class AllHeroesArrayComparator : IComparer {
	int IComparer.Compare (object dungeonHeroObjectX, object dungeonHeroObjectY)
	{
        GameObject dungeonHeroGameObjX = (GameObject) dungeonHeroObjectX;
        GameObject dungeonHeroGameObjY = (GameObject) dungeonHeroObjectY;
        DungeonHero dungeonHeroX = dungeonHeroGameObjX.GetComponent<DungeonHero> ();
        DungeonHero dungeonHeroY = dungeonHeroGameObjY.GetComponent<DungeonHero> ();
        if (dungeonHeroX.heroNumber == dungeonHeroY.heroNumber) {
            return 0;
		} else if (dungeonHeroX.heroNumber > dungeonHeroY.heroNumber){
            return 1;
		} else {
            return -1;
		}
	}
}

public class HeroSelectProbabilityComparator : IComparer {
    int IComparer.Compare (object heroSelectProbabilityX, object HeroSelectProbabilityY)
    {
        HeroSelectProbability heroProbX = (HeroSelectProbability)heroSelectProbabilityX;
        HeroSelectProbability heroProbY = (HeroSelectProbability)HeroSelectProbabilityY;
        if (heroProbX.heroSelectionProb == heroProbY.heroSelectionProb) {
            return 0;
        } else if (heroProbX.heroSelectionProb < heroProbY.heroSelectionProb) {
            return 1;
        } else {
            return -1;
        }
    }
}
