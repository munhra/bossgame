﻿using UnityEngine;
using UnityEditor;

public class DungeonAnimationsBuilder : MonoBehaviour {


#if UNITY_EDITOR

	public bool redefineAnimationsRoomAnimations = false;
	public bool redefineAnimationsHeroAnimations = true;

	public const string ROOM_ANIMATION_PATH = "assets/Resources/Animations/Animations/dugeonRoomAnimation/DungeonRoom";
	public const string HERO_ANIMATION_PATH = "assets/Resources/Animations/Animations/dungeonHeroAnimation/DungeonHero";

	void createAnimationFile(ObjectReferenceKeyframe[] spriteKeyFrames, int i, string fileName, int animIndex, string path, float frameRate) {

		AnimationClip animationClip = new AnimationClip ();

		animationClip.frameRate = frameRate;

		EditorCurveBinding spriteBinding = new EditorCurveBinding ();
		spriteBinding.type = typeof(SpriteRenderer);
		spriteBinding.path = "";
		spriteBinding.propertyName = "m_Sprite";

		AnimationUtility.SetObjectReferenceCurve(animationClip, spriteBinding, spriteKeyFrames);
		AnimationClipSettings animationClipSettings = AnimationUtility.GetAnimationClipSettings (animationClip);
		animationClipSettings.loopTime = true;

		AnimationUtility.SetAnimationClipSettings (animationClip, animationClipSettings);

		AssetDatabase.CreateAsset(animationClip, path+i+"/"+fileName);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();

	}

    private void createOneFrameAnimation(string fileName, int roomIndex, int frameIndex, Sprite roomSprite)
    {
        ObjectReferenceKeyframe[]  spriteKeyFrames = new ObjectReferenceKeyframe[1];
        spriteKeyFrames[0].time = 0;
        spriteKeyFrames[0].value = roomSprite;
        createAnimationFile(spriteKeyFrames, roomIndex, fileName, frameIndex, ROOM_ANIMATION_PATH, 4.0f);
    }

	private void createTwoFrameAnimation (string fileName, int roomIndex, int frameIndex, Sprite roomSprite1, Sprite roomSprite2)
    {
        ObjectReferenceKeyframe[] spriteKeyFrames;
        spriteKeyFrames = new ObjectReferenceKeyframe[2];
        spriteKeyFrames[0].time = 0;
        spriteKeyFrames[0].value = roomSprite1;
        spriteKeyFrames[1].time = 0.50f;
        spriteKeyFrames[1].value = roomSprite2;
        createAnimationFile(spriteKeyFrames, roomIndex, fileName, frameIndex, ROOM_ANIMATION_PATH, 4.0f);

    }

    void createAnimationFramesForDungeonRooms()
    {
        Sprite[] roomSmallSprites;
        Sprite[] roomMediumSprites;
        Sprite[] roomLargeSprites;

        string[] roomClasses = { "small", "medium", "large" };

        for (int i = 1; i < 19; i++)
        {
            roomSmallSprites = Resources.LoadAll<Sprite>("Sprites\\Rooms\\Room" +i+"\\"+roomClasses[0]);
            roomMediumSprites = Resources.LoadAll<Sprite>("Sprites\\Rooms\\Room" + i + "\\" + roomClasses[1]);
            roomLargeSprites = Resources.LoadAll<Sprite>("Sprites\\Rooms\\Room" + i + "\\" + roomClasses[2]);



            for (int j = 0; j < (roomSmallSprites.Length); j++)
            {
                //ObjectReferenceKeyframe[] spriteKeyFrames;
                string filename = "";

                // 00 attack
                // 01 dead
                // 02 defend
                // 03 hit
                // 04 idle A
                // 05 idle B

                switch (j)
                {

                    case 0:
                        filename = "SmallDungeonRoom" + i + "Attack.anim";
                        createOneFrameAnimation(filename, i, j, roomSmallSprites[j]);
                        filename = "MediumDungeonRoom" + i + "Attack.anim";
                        createOneFrameAnimation(filename, i, j, roomMediumSprites[j]);
                        filename = "LargeDungeonRoom" + i + "Attack.anim";
                        createOneFrameAnimation(filename, i, j, roomLargeSprites[j]);
                        break;
                    case 1:
                        filename = "SmallDungeonRoom" + i + "Die.anim";
                        createOneFrameAnimation(filename, i, j, roomSmallSprites[j]);
                        filename = "MediumDungeonRoom" + i + "Die.anim";
                        createOneFrameAnimation(filename, i, j, roomMediumSprites[j]);
                        filename = "LargeDungeonRoom" + i + "Die.anim";
                        createOneFrameAnimation(filename, i, j, roomLargeSprites[j]);
                        break;
                    case 2:
                        filename = "SmallDungeonRoom" + i + "Defend.anim";
                        createOneFrameAnimation(filename, i, j, roomSmallSprites[j]);
                        filename = "MediumDungeonRoom" + i + "Defend.anim";
                        createOneFrameAnimation(filename, i, j, roomMediumSprites[j]);
                        filename = "LargeDungeonRoom" + i + "Defend.anim";
                        createOneFrameAnimation(filename, i, j, roomLargeSprites[j]);
                        break;
                    case 3:
                        filename = "SmallDungeonRoom" + i + "Hit.anim";
                        createOneFrameAnimation(filename, i, j, roomSmallSprites[j]);
                        filename = "MediumDungeonRoom" + i + "Hit.anim";
                        createOneFrameAnimation(filename, i, j, roomMediumSprites[j]);
                        filename = "LargeDungeonRoom" + i + "Hit.anim";
                        createOneFrameAnimation(filename, i, j, roomLargeSprites[j]);
					    break;
                    case 4:
                        filename = "SmallDungeonRoom" + i + "Idle.anim";
                        createTwoFrameAnimation(filename, i, j, roomSmallSprites[j], roomSmallSprites[j + 1]);
                        filename = "MediumDungeonRoom" + i + "Idle.anim";
                        createTwoFrameAnimation(filename, i, j, roomMediumSprites[j], roomMediumSprites[j + 1]);
                        filename = "LargeDungeonRoom" + i + "Idle.anim";
                        createTwoFrameAnimation(filename, i, j, roomLargeSprites[j], roomLargeSprites[j + 1]);
					    break;
                }

            }

        }
    }
    
	void createAnimationFrames(AnimationType animationType) {

		Sprite[] roomSprites;
		int totalSprites;

		if (animationType == AnimationType.DUNGEON_ROOM) {
			totalSprites = 19;
		} else {
			totalSprites = 51;
		}
			
		for (int i = 1; i < totalSprites; i++) {

			if (animationType == AnimationType.DUNGEON_ROOM) {
				roomSprites = Resources.LoadAll<Sprite> ("Sprites\\Rooms\\Room"+i);
			} else {
				roomSprites = Resources.LoadAll<Sprite> ("Sprites\\heroes\\hero"+i);
			}

		
			for(int j = 0; j < (roomSprites.Length); j++) {
				ObjectReferenceKeyframe[] spriteKeyFrames;
				string filename = "";

				// 00 attack
				// 01 dead
				// 02 defend
				// 03 hit
				// 04 idle A
				// 05 idle B

				switch (j) {

				case 0:

					if (animationType == AnimationType.DUNGEON_ROOM) {
						filename = "DungeonRoom" + i + "Attack.anim";
					} else {
						if (i < 10) {
							filename = "Hero 0" + i + " attack.anim";
						} else {
							filename = "Hero " + i + " attack.anim";
						}
					}

					spriteKeyFrames = new ObjectReferenceKeyframe[1];
					spriteKeyFrames [0].time = 0;
					spriteKeyFrames [0].value = roomSprites [j];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						createAnimationFile (spriteKeyFrames, i, filename, j, ROOM_ANIMATION_PATH, 4.0f);
					} else {
						createAnimationFile (spriteKeyFrames, i, filename, j, HERO_ANIMATION_PATH, 12.0f);
					}
						
					break;
				case 1:

					if (animationType == AnimationType.DUNGEON_ROOM) {
						filename = "DungeonRoom" + i + "Die.anim";
					} else {
						if (i < 10) {
							filename = "Hero 0" + i + " dead.anim";
						} else {
							filename = "Hero " + i + " dead.anim";
						}
					}
						
					spriteKeyFrames = new ObjectReferenceKeyframe[1];
					spriteKeyFrames [0].time = 0;
					spriteKeyFrames [0].value = roomSprites [j];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						createAnimationFile (spriteKeyFrames, i, filename, j, ROOM_ANIMATION_PATH, 4.0f);
					} else {
						createAnimationFile (spriteKeyFrames, i, filename, j, HERO_ANIMATION_PATH, 12.0f);
					}

					break;
				case 2:
					
					if (animationType == AnimationType.DUNGEON_ROOM) {
						filename = "DungeonRoom" + i + "Defend.anim";
					} else {
						if (i < 10) {
							filename = "Hero 0" + i + " defence.anim";
						} else {
							filename = "Hero " + i + " defence.anim";
						}
					}

					spriteKeyFrames = new ObjectReferenceKeyframe[1];
					spriteKeyFrames [0].time = 0;
					spriteKeyFrames [0].value = roomSprites [j];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						createAnimationFile (spriteKeyFrames, i, filename, j, ROOM_ANIMATION_PATH, 4.0f);
					} else {
						createAnimationFile (spriteKeyFrames, i, filename, j, HERO_ANIMATION_PATH, 12.0f);
					}
						
					break;
				case 3:
					
					if (animationType == AnimationType.DUNGEON_ROOM) {
						filename = "DungeonRoom" + i + "Hit.anim";
					} else {
						if (i < 10) {
							filename = "Hero 0" + i + " hit.anim";
						} else {
							filename = "Hero " + i + " hit.anim";
						}
					}
						
					spriteKeyFrames = new ObjectReferenceKeyframe[1];
					spriteKeyFrames [0].time = 0;
					spriteKeyFrames [0].value = roomSprites [j];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						createAnimationFile (spriteKeyFrames, i, filename, j, ROOM_ANIMATION_PATH, 4.0f);
					} else {
						createAnimationFile (spriteKeyFrames, i, filename, j, HERO_ANIMATION_PATH, 12.0f);
					}

					break;
				case 4:
					
					if (animationType == AnimationType.DUNGEON_ROOM) {
						filename = "DungeonRoom" + i + "Idle.anim";
					} else {
						if (i < 10) {
							filename = "Hero 0" + i + " idle.anim";
						} else {
							filename = "Hero " + i + " idle.anim";
						}
					}

					spriteKeyFrames = new ObjectReferenceKeyframe[2];
					spriteKeyFrames [0].time = 0;
					spriteKeyFrames [0].value = roomSprites [j];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						spriteKeyFrames [1].time = 0.50f;
					} else {
						spriteKeyFrames [1].time = 0.08f;
					}
						
					spriteKeyFrames [1].value = roomSprites [j + 1];

					if (animationType == AnimationType.DUNGEON_ROOM) {
						createAnimationFile (spriteKeyFrames, i, filename, j, ROOM_ANIMATION_PATH, 4.0f);
					} else {
						createAnimationFile (spriteKeyFrames, i, filename, j, HERO_ANIMATION_PATH, 12.0f);
					}
						
					break;
				}

			}
		}
	}

	void Start () {
		if (redefineAnimationsRoomAnimations) {
            //createAnimationFrames (AnimationType.DUNGEON_ROOM);
            createAnimationFramesForDungeonRooms();
		}
		if (redefineAnimationsHeroAnimations) {
            //create animation frame will only be used for dungeon hero
            createAnimationFrames (AnimationType.DUNGEON_HERO);
		}
	}

#endif
}


public enum AnimationType {

	DUNGEON_HERO,
	DUNGEON_ROOM

}