﻿using UnityEngine;

public class DungeonLogoFront : MonoBehaviour {
	void moveDungeonLogoFront () {
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeInBounce, "time",1.0f, "alpha", 255.0f);
		iTween.FadeTo (this.gameObject, iTweenLogoHash);
	}
}
