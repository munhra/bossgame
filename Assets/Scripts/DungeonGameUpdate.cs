﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DungeonGameUpdate : MonoBehaviour {
	public Button playGameButton;
	private List<Object> dungeonRoomArray;
	public GameObject smallLocked1;
	public GameObject smallLocked2;
	public GameObject smallLocked3;
	public GameObject largeLocked;
	public GameObject gaugeLVL1pts;
	public GameObject gaugeLVL2pts;
	public GameObject gaugeLVL3pts;
	public GameObject redGauge;
	//public GameObject largeLevelValue;
	public GameObject level1GaugePointer;
	public GameObject level2GaugePointer;
	public GameObject level3GaugePointer;
	public DungeonGameSoundManager dungeonSoundManager;

	void Start ()
	{
		dungeonSoundManager = GameObject.Find ("dungeonSoundManager").GetComponent<DungeonGameSoundManager> ();
		playGameButton.onClick.AddListener (playButtonOnClick);
		//configureLargeLevelValue ();
		configureLockedIcons (0);
		configureLargeLock (true);
		configureGaugeTexts ("0", "19", "31");
	}

	void playButtonOnClick ()
	{
		DungeonGameManager dungeonGameManager = GameObject.Find ("GameManager").GetComponent<DungeonGameManager> ();
		if (dungeonGameManager.gameState == DungeonGameManager.GameState.GAME_MONSTER_UP) {
			dungeonGameManager.performGameResumeAfterMonsterUp ();
			playGameButton.transform.position = new Vector3 (-10.0f, playGameButton.transform.position.y, 0);
		}
		dungeonSoundManager.stopAudio ();
		dungeonSoundManager.playButtonClick ();
		dungeonSoundManager.playRandomLevelMusic ();
	}

	public void setProgressIndicatorValuePosition (float level1Progress, float level2Progress)
	{
		gaugeLVL1pts.transform.position = new Vector2 (gaugeLVL1pts.transform.position.x, -3.45f + level1Progress * 6.8f);
		gaugeLVL2pts.transform.position = new Vector2 (gaugeLVL2pts.transform.position.x, -3.45f + level2Progress * 6.8f);
	}

	public void setProgressIndicators (float level1Progress, float level2Progress)
	{
		level1GaugePointer.transform.position = new Vector2 (level1GaugePointer.transform.position.x, -3.45f + level1Progress * 6.8f);
		level2GaugePointer.transform.position = new Vector2 (level2GaugePointer.transform.position.x, -3.45f + level2Progress * 6.8f);
	}

	public void setProgressGauge (float progress)
	{
		var iTweenSwordHash = iTween.Hash ("easetype", iTween.EaseType.easeOutExpo, "time", 1.6f, "y", -6.8f + progress * 6.8f);
		iTween.MoveTo (redGauge, iTweenSwordHash);
		showPlayButton ();
	}

	void showPlayButton()
	{
		var iTweenLogoHash = iTween.Hash ("easetype", iTween.EaseType.easeOutBack, "time", 0.5f, "x", 0.20f);
		iTween.MoveTo (playGameButton.gameObject, iTweenLogoHash);
	}

	//void configureLargeLevelValue ()
	//{
	//	Text largeLevelValueText = largeLevelValue.GetComponentInChildren<Text> ();
	//}

	public void configureGaugeTexts (string value1, string value2, string value3)
	{
		Text gaugeLVL1Text = gaugeLVL1pts.GetComponentInChildren<Text> ();
		Text gaugeLVL2Text = gaugeLVL2pts.GetComponentInChildren<Text> ();
		Text gaugeLVL3Text = gaugeLVL3pts.GetComponentInChildren<Text> ();
		gaugeLVL1Text.text = value1;
		gaugeLVL2Text.text = value2;
		gaugeLVL3Text.text = value3;
	}

	void configureLargeLock (bool open)
	{
		largeLocked.GetComponent<Renderer> ().enabled = !open;
	}

	public void lockAllIcons()
	{
		Debug.Log ("lockAllIcons");
		GameObject [] lockedIconArray = { smallLocked1, smallLocked2, smallLocked3 };
		for (int i = 0; i < 3; i++) {
			lockedIconArray [i].GetComponent<Renderer> ().enabled = true;
		}
	}

	public void configureLockedIcons (int numberToUnlock)
	{
		GameObject [] lockedIconArray = { smallLocked1, smallLocked2, smallLocked3 };
		for (int i = 0; i < numberToUnlock; i++) {
			lockedIconArray [i].GetComponent<Renderer> ().enabled = false;
		}
	}

	public void buildGameOverLargeCard (string dungeonName)
	{
		GameObject largeMonsterCard = GameObject.Find ("mockGameOverCardLarge");
		Destroy (largeMonsterCard);
		dungeonRoomArray = new List<Object> ();
		Object loadedDungeonRoom = Resources.Load<GameObject> (dungeonName);
		dungeonRoomArray.Add (loadedDungeonRoom);
		GameObject clone = Instantiate (dungeonRoomArray [0]) as GameObject;
		clone.tag = "dungeonCardLarge";
		clone.layer = 11;
		clone.transform.parent = this.transform;
		clone.transform.position = new Vector3 (0.83f, 19.6f, 0.0f);
		clone.transform.localScale = new Vector3 (2.5f, 2.5f, 0.0f);
		clone.GetComponent<SpriteRenderer> ().sortingLayerName = "GameOverCardsLayer";
		clone.name = "mockGameOverCardLarge";
	}

	/// <summary>
	/// This method loads the 3 sprites that will be offerd as monster upgrade, they will be show on the 3 spaces on top
	/// of this view
	/// </summary>
	public void buildGameOverUpgradeRoomCards (string [] prefabs)
	{
		for (int i = 0; i < 3; i++) {
			GameObject monsterCard = GameObject.Find ("mockGameOverCard_" + i);
			Destroy (monsterCard);
		}

		dungeonRoomArray = new List<Object> ();
		for (int i = 0; i < 3; i++) {
			Object loadedDungeonRoom = Resources.Load<GameObject> (prefabs [i]);
			dungeonRoomArray.Add (loadedDungeonRoom);
		}
		float [] multiplierSelect = { -3.10f, 0, 3.10f };
		for (int j = 0; j < 3; j++) {
			GameObject clone;
			clone = Instantiate (dungeonRoomArray [j]) as GameObject;
			clone.tag = "dungeonGameOver";
			clone.layer = 11;
			clone.transform.parent = this.transform;
			clone.transform.position = new Vector3 (multiplierSelect [j], 24.70f, 0.0f);
			clone.GetComponent<SpriteRenderer> ().sortingLayerName = "GameOverCardsLayer";
			clone.name = "mockGameOverCard_" + j;

		}
	}
}
