﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class DungeonGameData : MonoBehaviour
{
    public Dictionary<string, DungeonHeroData> dungeonHeroDataDict = new Dictionary<string, DungeonHeroData> ();
    public Dictionary<string, DungeonRoomData> dungeonRoomDataDict = new Dictionary<string, DungeonRoomData> ();

    public TextAsset dungeonHeroDataFile;
    public TextAsset dungeonRoomDataFile;
   
    public void loadDungeonHeroData()
	{
        string dungeonHeroTextData = dungeonHeroDataFile.text;
        string [] dataLines = dungeonHeroTextData.Split (char.Parse ("\n"));
        for (int i = 1; i < dataLines.Length; i++) {
            string [] stringHeroData = dataLines [i].Split (char.Parse (","));
            DungeonHeroData dungeonHeroData = new DungeonHeroData ();
            string dungeonHeroName = stringHeroData [0];
            dungeonHeroData.heroLife = Int32.Parse (stringHeroData [1]);
            dungeonHeroData.heroTotalDamage = Int32.Parse (stringHeroData [2]);
            dungeonHeroData.heroGold = Int32.Parse (stringHeroData [3]);
            dungeonHeroData.heroDefenceChance = Int32.Parse (stringHeroData [4]);
            dungeonHeroData.heroSpeed = float.Parse (stringHeroData [5]);
            dungeonHeroData.heroMaxTries = Int32.Parse (stringHeroData [6]);
            dungeonHeroDataDict.Add (dungeonHeroName, dungeonHeroData);
        }
    }

    public void loadDungeonRoomData()
	{
        string dungeonRoomTextData = dungeonRoomDataFile.text;
        string [] dataLines = dungeonRoomTextData.Split (char.Parse ("\n"));
        for (int i = 1; i < dataLines.Length; i++) {
            string [] stringRoomData = dataLines [i].Split (char.Parse (","));
            DungeonRoomData dungeonRoomData = new DungeonRoomData ();
            dungeonRoomData.dungeonRoomName = stringRoomData [0];
            dungeonRoomData.dungeonRoomArmor = Int32.Parse (stringRoomData [1]);
            dungeonRoomData.dungeonLife = dungeonRoomData.dungeonRoomArmor;
            dungeonRoomData.dungeonRoomDamage = Int32.Parse (stringRoomData [2]);
            dungeonRoomData.dungeonDefenceChance = Int32.Parse (stringRoomData [3]);
            dungeonRoomData.dungeonGoldPrice = Int32.Parse (stringRoomData [4]);
            dungeonRoomDataDict.Add (dungeonRoomData.dungeonRoomName, dungeonRoomData);
        }
    }
}

public struct DungeonHeroData {
    public int heroLife;
    public int heroTotalDamage;
    public int heroGold;
    public int heroDefenceChance;
    public float heroSpeed;
    public int heroMaxTries;
}

public struct DungeonRoomData {
    public string dungeonRoomName;
    public int dungeonRoomArmor;
    public int dungeonRoomDamage;
    public int dungeonLife;
    public int dungeonDefenceChance;
    public int dungeonGoldPrice;
}
